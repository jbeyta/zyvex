<?php
/**
 * Load related files
 * Project: User Role Editor Pro WordPress plugin
 * 
 * Author: Vladimir Garagulya
 * email: support@role-editor.com
 *
**/

require_once(URE_PLUGIN_DIR .'includes/pro/classes/utils.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/mutex.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/license-key.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/bbpress.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/assign-role-pro.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/screen-help-pro.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/access-ui-controller.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/create-posts-cap.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/post-types-own-caps.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/export-import.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/shortcodes.php');

// Additional modules:

// Admin menu access
require_once( URE_PLUGIN_DIR .'includes/pro/classes/admin-menu-copy.php');
require_once( URE_PLUGIN_DIR .'includes/pro/classes/admin-menu-hashes.php');
require_once( URE_PLUGIN_DIR .'includes/pro/classes/admin-menu.php');
require_once( URE_PLUGIN_DIR .'includes/pro/classes/admin-menu-view.php');
require_once( URE_PLUGIN_DIR .'includes/pro/classes/admin-menu-access.php');

// Widgets access
require_once( URE_PLUGIN_DIR .'includes/pro/classes/widgets.php');
require_once( URE_PLUGIN_DIR .'includes/pro/classes/widgets-access.php');

// Metaboxes access
require_once( URE_PLUGIN_DIR .'includes/pro/classes/meta-boxes.php');
require_once( URE_PLUGIN_DIR .'includes/pro/classes/meta-boxes-access.php');

// Other Roles access
require_once( URE_PLUGIN_DIR .'includes/pro/classes/other-roles.php');
require_once( URE_PLUGIN_DIR .'includes/pro/classes/other-roles-access.php');

// Posts edit access
require_once(URE_PLUGIN_DIR .'includes/pro/classes/posts-edit-access-role.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/posts-edit-access-user-meta.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/posts-edit-access-user.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/posts-edit-access-bulk-action.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/posts-edit-access.php');            

// Plugins activation access
require_once(URE_PLUGIN_DIR .'includes/pro/classes/plugins-activation-access.php');

// Themes activation access
require_once(URE_PLUGIN_DIR .'includes/pro/classes/themes-access.php');

// Gravity Forms Access
require_once(URE_PLUGIN_DIR .'includes/pro/classes/gf-access.php');

// Content view restricitons
require_once( URE_PLUGIN_DIR .'includes/pro/classes/posts-view.php');
require_once( URE_PLUGIN_DIR .'includes/pro/classes/posts-view-access.php');
require_once( URE_PLUGIN_DIR .'includes/pro/classes/content-view-restrictions.php');


require_once(URE_PLUGIN_DIR .'includes/pro/classes/user-role-editor-pro-view.php');
require_once(URE_PLUGIN_DIR .'includes/pro/classes/user-role-editor-pro.php');