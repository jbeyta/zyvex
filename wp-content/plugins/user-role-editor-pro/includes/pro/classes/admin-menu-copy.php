<?php
/*
 * User Role Editor Pro WordPress plugin
 * Class URE_Admin_Menu_Copy - creates/stores updated copy of WP admin backend menu for use in admin menu access add-on
 * Author: Vladimir Garagulya
 * Author email: support@role-editor.com
 * Author URI: https://role-editor.com
 * License: GPL v2+ 
 */
class URE_Admin_Menu_Copy {

    private static $admin_is_parent = false;
    
    
    private static function get_menu_hook($menu_key, $submenu_item) {
        
        if (!empty($submenu_item)) {
            $menu_hook = get_plugin_page_hook($submenu_item[2], $menu_key);
        } else {
            $menu_hook = '';
        }
        
        return $menu_hook;
    }
    // end of get_menu_hook()
    
    
    private static function get_menu_file($submenu_item) {
        if (!empty($submenu_item)) {
            $menu_file = $submenu_item[2];
            $pos = strpos($menu_file, '?');
            if ($pos!==false) {
                $menu_file = substr($menu_file, 0, $pos);
            }
        } else {
            $menu_file = '';
        }
    }
    // end of get_menu_file()
        
    
    private static function get_menu_item_link($menu_key, $submenu_as_parent = true) {
        
        global $submenu;
        
        $link = '';
        $submenu_item = '';
        if (!empty($submenu[$menu_key])) {			
            $submenu_items = $submenu[$menu_key];
            $submenu_item = reset($submenu_items);  // get 1st element of array
        }
        
        $menu_hook = self::get_menu_hook($menu_key, $submenu_item);
        $menu_file = self::get_menu_file($submenu_item);        
        if ($submenu_as_parent && !empty($submenu_item)) {
            if (!empty($menu_hook) || 
                (('index.php'!=$submenu_item[2]) && file_exists(WP_PLUGIN_DIR .'/'. $menu_file) && 
                 !file_exists(ABSPATH .'/wp-admin/'. $menu_file))) {
                self::$admin_is_parent = true;
                $link = 'admin.php?page='. $submenu_item[2];
            } else {
                $link = $submenu_item[2];
            }
        } elseif (!empty($menu_key)) {
            $menu_hook = get_plugin_page_hook( $menu_key, 'admin.php');
            $menu_file = $menu_key;
            $pos = strpos($menu_file, '?');
            if (false!==$pos) {
                $menu_file = substr($menu_file, 0, $pos);
            }
            if (!empty($menu_hook) || 
                (('index.php'!=$menu_key ) && file_exists(WP_PLUGIN_DIR .'/'. $menu_file) && 
                 !file_exists(ABSPATH .'/wp-admin/'. $menu_file))) {                
                self::$admin_is_parent = true;
                $link = 'admin.php?page='. $menu_key;
            } else {
                $link = $menu_key;
            }
        }

        $normalized_link = URE_Admin_Menu::normalize_link($link);
        
        return $normalized_link;
    }
    // end of get_menu_link()
       
    
    private static function get_submenu_item_link($submenu_key, $menu_key) {
        
        $link = '';
        $menu_file = $menu_key;
        $pos = strpos($menu_file, '?');
        if ($pos!==false) {
            $menu_file = substr($menu_file, 0, $pos);
        }

        $menu_hook = get_plugin_page_hook($submenu_key, $menu_key);
    				$sub_file = $submenu_key;
        $pos = strpos($sub_file, '?');
        if ($pos!==false) {
            $sub_file = substr($sub_file, 0, $pos);
        }
        if (!empty($menu_hook) || 
            (('index.php'!=$submenu_key) && file_exists(WP_PLUGIN_DIR .'/'. $sub_file) && 
              !file_exists(ABSPATH .'/wp-admin/'. $sub_file))) {
            // If admin.php is the current page or if the parent exists as a file in the plugins or admin dir
            if ((!self::$admin_is_parent && file_exists(WP_PLUGIN_DIR .'/'. $menu_file) && 
                 !is_dir(WP_PLUGIN_DIR .'/'. $menu_key)) || file_exists($menu_file)) {
                $link = add_query_arg(array('page' => $submenu_key), $menu_key);
            } else {
                $link = add_query_arg(array('page' => $submenu_key), 'admin.php');
            }
            $link = esc_url($link);
        } else {
            $link = $submenu_key;
        }
        
        $normalized_link = URE_Admin_Menu::normalize_link($link);
        
        return $normalized_link;
    }
    // end of get_submenu_item_link()
    

    /**
     * Save current WordPress admin menu for future use via AJAX requests, when menu is not available
     * 
     */
    public static function update() {
        global $menu, $submenu;
        
        $menu_hashes = array();        
        $menu_copy = $menu;
        $submenu_copy = $submenu;
        foreach($menu_copy as $key=>$menu_item) {
            self::$admin_is_parent = false;
            if ($menu_item[4]==='wp-menu-separator' || $menu_item[4]==='separator-woocommerce') {   // do not include separators
                unset($menu_copy[$key]);
                continue;
            }
            for($i=3; $i<count($menu_item); $i++) {
                unset($menu_copy[$key][$i]);
            }
            
            $menu_key = $menu_item[2];
            $link = self::get_menu_item_link($menu_key);
            $menu_copy[$key][3] = $link;
            $menu_hashes[$link] = 1;
            
            if (empty($submenu[$menu_key])) {    // Menu does not have submenu
                continue;
            }
            
            // Go through submenu
            foreach($submenu_copy[$menu_key] as $key1=>$items) {
                $link = self::get_submenu_item_link($items[2], $menu_key);
                $submenu_copy[$menu_key][$key1][3] = $link;
                $menu_hashes[$link] = 1;
            }
        }
                
        update_option(URE_ADMIN_MENU::ADMIN_MENU_COPY_KEY, $menu_copy);
        update_option(URE_ADMIN_MENU::ADMIN_SUBMENU_COPY_KEY, $submenu_copy);
        update_option(URE_ADMIN_MENU::ADMIN_MENU_HASHES, $menu_hashes);
        
    }
    // end of update_menu_copy()
    
}
// end of Admin_Menu_Copy class