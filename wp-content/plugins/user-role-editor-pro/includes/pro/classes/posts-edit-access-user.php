<?php
/*
 * Class: Access restrictions to posts/pages for user
 * Project: User Role Editor Pro WordPress plugin
 * Author: Vladimir Garagulya
 * email: vladimir@shinephp.com
 * 
 */

class URE_Posts_Edit_Access_User {
    
    private $lib = null;    // Ure_Lib_Pro class instance
    private $pea = null;    // URE_Posts_Edit_Access class instance
    private $user_meta = null;    // user meta keys
    private $role = null;   // URE_Posts_Edit_Access_Role instance
    
    private $posts_list = null;
    private $attachments_list = null;    
    
    private $restricted = null; // true if edit restrictions are set for the user - it's used in case posts list available is empty
    
    
    public function __construct(Ure_Lib_Pro $lib, URE_Posts_Edit_Access $pea) {
        
        $this->lib = $lib;  
        $this->pea = $pea;
        $this->user_meta = new URE_Posts_Edit_Access_User_Meta();
        // $this->role = new URE_Posts_Edit_Access_Role($lib);
        
        add_action('edit_user_profile', array($this, 'edit_user_posts_list'), 10, 2);
        add_action('profile_update', array($this, 'save_user_restrictions'), 10);
        
        new URE_Posts_Edit_Access_Bulk_Action($lib);
        
    }
    // end of __construct()
        
    
    /**
     * Returns true if user can edit posts or page or any custom post type
     * 
     * @param WP_User $user
     * @return boolean
     */
    private function can_edit_content($user) {
            
        $caps = $this->lib->get_edit_custom_post_type_caps();
        $min_cap = $this->lib->user_can_which($user, $caps);
        if (empty($min_cap)) {
            return false;
        }
        
        return true;
    }
    // end of can_edit_content()
    
    
    public function is_restriction_applicable() {
        
        global $current_user;
        
        // do not restrict administrators
        if ( $this->lib->user_is_admin($current_user->ID) ) {
            return false;
        }
                        
        $show_full_list = apply_filters('ure_posts_show_full_list', false);
        if ($show_full_list) { // show full list of post/pages/custom post types
            return false;
        }
    
        // do not restrict users without edit posts/pages/custom post types capabilities
        if (!$this->can_edit_content($current_user)) {
            return false;
        }
        
        // do not apply restrictions if the posts/pages edit restriction is not set for this user
        if (!$this->is_restricted()) {
            return false;
        }
  
        return true;
    }
    // end of is_restriction_applicable()
    
    
    public function edit_user_posts_list($user) {

        $result = stripos($_SERVER['REQUEST_URI'], 'network/user-edit.php');
        if ($result !== false) {  // exit, this code just for single site user profile only, not for network admin center
            return;
        }
        
        // do not restrict administrators
        if ( $this->lib->user_is_admin($user->ID) ) {
            return false;
        }
        
        if (!$this->can_edit_content($user)) {
            return false;
        }
        
        if (!current_user_can('ure_edit_posts_access')) {
            return;
        }
        
        // by post ID
        $restriction_type = $this->user_meta->get_restriction_type($user->ID);
        $checked1 = ($restriction_type==1) ? 'checked' : '';
        $checked2 = ($restriction_type==2) ? 'checked' : '';
        $posts_list = $this->user_meta->get_posts_list($user->ID);
        
        // be category/taxonomy
        $categories_list = $this->user_meta->get_post_categories_list($user->ID);
        
        // by post author
        $post_authors_list = $this->user_meta->get_post_authors_list($user->ID);
        
        $caps = $this->lib->get_edit_custom_post_type_caps();
        if ( $this->lib->user_can_which( $user, $caps) ) {        
?>        
        <h3><?php _e('Posts/Pages/Custom Post Types Editor Restrictions', 'user-role-editor'); ?></h3>
        <table class="form-table">
            <tr>
                <th scope="role" colspan="2">
                    <input type="radio" name="ure_posts_restriction_type" id="ure_posts_restriction_type1" value="1" <?php  echo $checked1;?> >
                    <label for="ure_posts_restriction_type1"><?php esc_html_e('Allow', 'user-role-editor'); ?></label>&nbsp;
                    <input type="radio" name="ure_posts_restriction_type" id="ure_posts_restriction_type2" value="2" <?php  echo $checked2;?> >
                    <label for="ure_posts_restriction_type2"><?php esc_html_e('Prohibit', 'user-role-editor'); ?></label>
                    to edit posts/pages/custom post types:
                <th>
            </tr>    
        		<tr>
        			<th scope="row">               
               <?php esc_html_e('with post ID (comma separated) ', 'user-role-editor'); ?>
           </th>
        			<td>
               <input type="text" name="ure_posts_list" id="ure_posts_list" value="<?php echo $posts_list; ?>" size="40" />
        			</td>
        		</tr>    
          <tr>
        			<th scope="row">               
               <?php esc_html_e('with category/taxonomy ID (comma separated) ', 'user-role-editor'); ?>
           </th>
        			<td>
               <input type="text" name="ure_categories_list" id="ure_categories_list" value="<?php echo $categories_list; ?>" size="40" />
        			</td>
        		</tr>
<?php
            if ( $this->lib->user_can_which( $user, array('edit_others_posts', 'edit_others_pages') ) ) {
?>
          <tr>
        			<th scope="row">
               <?php esc_html_e('with author user ID (comma separated) ', 'user-role-editor'); ?>
           </th>
        			<td>
               <input type="text" name="ure_post_authors_list" id="ure_post_authors_list" value="<?php echo $post_authors_list; ?>" size="40" />
        			</td>
        		</tr>
<?php
            }
?>
        </table>		                
<?php
        }
    }
    // end of set_user_posts_list()    
 
    
    // update posts edit by post ID restriction: comma separated posts IDs list
    private function update_posts_list($user_id) {
        
        if (!empty($_POST['ure_posts_list'])) {
            $posts_list = explode(',', trim($_POST['ure_posts_list']));
            if (count($posts_list)>0) {
                $posts_list_str = $this->lib->filter_int_array_to_str($posts_list);
                $this->user_meta->set_posts_list($user_id, $posts_list_str);
            }            
        } else {
            $this->user_meta->delete_posts_list($user_id);
        }        
        
    }
    // end of update_posts_list()
    
    
    // update comma separated categories/taxonomies ID list 
    private function update_categories_list($user_id) {
        
        if (!empty($_POST['ure_categories_list'])) {
            $categories_list = explode(',', trim($_POST['ure_categories_list']));
            if (count($categories_list)>0) {
                $categories_list_str = $this->lib->filter_int_array_to_str($categories_list);
                $this->user_meta->set_post_categories_list($user_id, $categories_list_str);                                
            }            
        } else {
            $this->user_meta->delete_post_categories_list($user_id);
        }
        
    }
    // end of update_categories_list()    
    
    
    // update posts edit by author ID restriction: comma separated authors IDs list
    private function update_authors_list($user_id) {
        
        if (!empty($_POST['ure_post_authors_list'])) {
            $authors_list = explode(',', trim($_POST['ure_post_authors_list']));
            if (count($authors_list)>0) {
                $post_authors_list_str = $this->lib->filter_int_array_to_str($authors_list);
                $this->user_meta->set_post_authors_list($user_id, $post_authors_list_str);
            }            
        } else {
            $this->user_meta->delete_post_authors_list($user_id);
        }
        
    }
    // end of update_authors_list()
    
    
    // save posts edit restrictions when user profile page is updated, as WordPress itself doesn't know about it
    public function save_user_restrictions($user_id) {
        
        if (!isset($_POST['ure_posts_restriction_type'])) {
            // 'profile_update' action was fired not from the 'user profile' page, so there is no data to update
            return;
        }
        
        if (!current_user_can('edit_users', $user_id) || !current_user_can('ure_edit_posts_access')) {
            // not enough permissions for this action
            return;
        }
        
        $restriction_type = $_POST['ure_posts_restriction_type'];
        if ($restriction_type!=1 && $restriction_type!=2) {  // sanitize user input
            $restriction_type = 1;
        }
        $this->user_meta->set_restriction_type($user_id, $restriction_type);
        
        $this->update_posts_list($user_id);
        $this->update_categories_list($user_id);
        $this->update_authors_list($user_id);                                                        
        
        // Remove this meta field as it is unused anymore. 
        // This line added at version 4.23 (2016-01-25), remove it after 2-3 of months as unneded.
        $this->user_meta->delete_post_types($user_id);
                
    }
    // end of save_user_restrictions()    
    
    
    public function get_restriction_type($user_id=0) {        
        
        // get from user meta
        $value = $this->user_meta->get_restriction_type($user_id);
        if (empty($value)) {
            // get restriction type from user's roles
            
        }
        
        if (empty($value)) {
            $value = 1;
        }
        
        return $value;
    }
    // end of get_restriction_type()
                    
    
    public function get_categories_list() {
        
        global $current_user;
        
        $list = $this->user_meta->get_post_categories_list($current_user->ID);
        
        return $list;
    }
    // end of get_categories_list()
    
    
    public function get_post_authors_list() {
        
        global $current_user;
        
        $list = $this->user_meta->get_post_authors_list($current_user->ID);
        
        return $list;
    }
    // end of get_post_authors_list()
    
    
    private function get_posts_list_by_authors() {
        
        global $current_user, $wpdb;
                
        $post_authors_list = $this->get_post_authors_list();
        if (empty($post_authors_list)) {
            return array();
        }
        $restriction_type = $this->get_restriction_type();
        if ($restriction_type==1) {   // allow
            $authors = explode(',', $post_authors_list);
            if (!in_array($current_user->ID, $authors)) {
                // add user himself to the authors list to allow him edit his own posts/pages
                $post_authors_list .= ', '. $current_user->ID;
            }
        }
        $query = "select ID
                    from {$wpdb->posts}
                    where post_author in ($post_authors_list) and post_status!='inherit' and post_status!='revision';";
        $post_ids = $wpdb->get_col($query);
        if (!is_array($post_ids)) {
            return array();
        }
        
        return $post_ids;
    }
    // end of get_posts_list_by_authors()
    
    
    private function get_post_categories_list() {
        
        global $current_user;
        
        $list = $this->user_meta->get_post_categories_list($current_user->ID);
        
        return $list;
    }
    // end of get_post_categories_list()
    
    
    private function get_posts_list_by_categories() {
        
        global $wpdb;
                
        $categories_list_str = $this->get_post_categories_list();
        if (empty($categories_list_str)) {
            return array();
        }
        $query = "select object_id from {$wpdb->term_relationships} where term_taxonomy_id in ($categories_list_str)";
        $post_ids = $wpdb->get_col($query);
        if (!is_array($post_ids)) {
            return array();
        }
        
        return $post_ids;
    }
    // end of get_posts_list_by_categories()
    
    
    /**
     * get page children
     * @param int $page_id
     * @param array $all_pages ('id=>parent')
     * @return array child pages ID
     */
    private function get_page_children($page_id, $all_pages) {
        
        $children = array();
        foreach($all_pages as $child_id=>$parent_id) {
            if ($parent_id==$page_id) {
                $children[] = $child_id;
            }
        }
        
        return $children;
    }
    // end of get_page_children()
    
        
    /**
     * Add child pages to the posts/pages list
     */
    private function add_child_pages() {
            
        $auto_access = apply_filters('ure_auto_access_child_pages', true);
        if (empty($auto_access)) {
            return;
        }
        
        // remove filter temporally to exclude recursion
        remove_filter('map_meta_cap', array($this->pea, 'block_edit_post'), 10);
        $args = array(
            'post_type' => 'page', 
            'fields' => 'id=>parent',
            'nopaging' => true,
            'suppress_filters'=>true);
        $wp_query = new WP_Query();        
        $all_pages = $wp_query->query($args);
        // restore filter back
        add_filter('map_meta_cap', array($this->pea, 'block_edit_post'), 10, 4);
        
        foreach($this->posts_list as $post_id) {
            $post = get_post($post_id);
            if (empty($post) || $post->post_type!=='page') {
                continue;
            }
            
            $children = $this->get_page_children($post_id, $all_pages);
            if (empty($children)) {
                continue;                
            }
            
            $this->posts_list = array_merge($this->posts_list, $children);            
        }   // foreach(...)
        
    }
    // end of add_child_pages()
    
    
    private function exclude_posts_which_can_not_edit() {
        
        global $current_user;

        $post_types = $this->lib->_get_post_types();        
        $list = array();
        for ($i=0; $i<count($this->posts_list); $i++) {
            $post = get_post($this->posts_list[$i]);
            if (empty($post)) {
                continue;
            }
            // can not check if user can edit this post_type if post_type was not registered yet, so assume that he can in this case
            // example - WooCommerce products
            if (!isset($post_types[$post->post_type]) || 
                user_can($current_user->ID, 'edit_post', $this->posts_list[$i])) {
                $list[] = $this->posts_list[$i];
            }
        }
        
        $this->posts_list = $list;
    }
    // end of exclude_posts_which_can_not_edit()
    
    
    public function get_posts_list() {
        
        global $current_user;
    
        if ($this->posts_list!==null) {
            return $this->posts_list;
        }
            
        $posts_list1 = array();
        $posts_list_str = $this->user_meta->get_posts_list($current_user->ID);
        if (!empty($posts_list_str)) {
            $posts_list1 = explode(',', $posts_list_str);
        }

        $posts_list2 = $this->get_posts_list_by_categories();
        $posts_list3 = $this->get_posts_list_by_authors();
        $this->posts_list = array_values(array_unique(array_merge($posts_list1, $posts_list2, $posts_list3)));
        if (count($this->posts_list)==0) {
            return $this->posts_list;
        }
        
        
        for ($i=0; $i<count($this->posts_list); $i++) {
            $this->posts_list[$i] = trim($this->posts_list[$i]);
        }
        $this->add_child_pages();        
                
        $restriction_type = $this->get_restriction_type();
        if ($restriction_type==1) { // for 'Allow' only, do not exclude anything if it's a blocked/prohibited list
            $show_posts_which_can_edit_only = apply_filters('ure_show_posts_which_can_edit_only_backend',  true);
            if ($show_posts_which_can_edit_only) {
                $this->exclude_posts_which_can_not_edit();
            }
        }
        
        return $this->posts_list;
    }
    // end of get_posts_list()
    
   
    /**
     * Returns true in case edit restrictions are set for this user
     * 
     */
    public function is_restricted() {
        
        global $current_user;
        
        if ($this->restricted!==null) {
            return $this->restricted;
        }
        
        $posts_list_str = $this->user_meta->get_posts_list($current_user->ID);
        $categories_list_str = $this->get_post_categories_list();
        $post_authors_list = $this->get_post_authors_list();
        $restrictions = trim($posts_list_str . $categories_list_str . $post_authors_list);
        $this->restricted = empty($restrictions) ? false : true;
        
        return $this->restricted;
    }
    // end of is_restricted()
    
    
    public function get_attachments_list() {
        
        global $wpdb, $current_user;
    
        if ($this->attachments_list!=null) {
            return $this->attachments_list;
        }
            
        $posts_list = $this->get_posts_list();
        if (!is_array($posts_list) || count($posts_list)==0) {
            return $this->attachments_list;
        }
            
        $restriction_type = $this->get_restriction_type();
        $parents_list = implode(',', $posts_list);
        $query = "SELECT ID from $wpdb->posts WHERE post_type='attachment' AND ". 
                    "(post_parent in ($parents_list)"; 
        if ($restriction_type==1) {   // Allow
            $query .= " OR (post_parent=0 AND post_author=$current_user->ID)";
        }
        $query .= ')';                
        $this->attachments_list = $wpdb->get_col($query);            
        
        return $this->attachments_list;
    }
    // end of get_attachments_list()

    
}
// end of URE_Posts_Edit_Access_User class