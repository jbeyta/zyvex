<?php
/*
 * Class: Edit access restrict to posts/pages for role
 * Project: User Role Editor Pro WordPress plugin
 * Author: Vladimir Garagulya
 * email: vladimir@shinephp.com
 * 
 */

class URE_Posts_Edit_Access_Role {

    const ACCESS_DATA_KEY = 'ure_posts_edit_access_data';
    const edit_posts_access_cap = 'ure_edit_posts_access';
    
    // reference to the code library object
    private $lib = null;        


    public function __construct($lib) {
        
        $this->lib = $lib;
        
        if (!(defined('DOING_AJAX') && DOING_AJAX)) {
            add_action('ure_role_edit_toolbar_service', array(&$this, 'add_toolbar_buttons'));
            add_action('ure_load_js', array(&$this, 'add_js'));
            add_action('ure_dialogs_html', array(&$this, 'dialog_html'));
            add_action('ure_process_user_request', array(&$this, 'update_data'));        
        }

    }
    // end of __construct()

    
    public function load_data($role_id) {
    
        $access_data = get_option(self::ACCESS_DATA_KEY);
        if (is_array($access_data) && array_key_exists($role_id, $access_data)) {
            $result =  $access_data[$role_id];            
        } else {
            $result = array(
                'restriction_type'=>1,
                'data'=>array(
                    'posts_list'=>array(),
                    'authors_list'=>array(),
                    'terms_list'=>array(),
                    'post_types'=>array()
                    ));
        }
        
        return $result;
        
    }
    // end of load_access_data_for_role()
    
        
    public function add_toolbar_buttons() {
        if (!current_user_can(self::edit_posts_access_cap)) {
            return;
        }
            
        $button_title = esc_html__('Allow/Prohibit editing selected posts', 'user-role-editor');
        $button_label = esc_html__('Posts Edit', 'user-role-editor');
?>                
        <button id="ure_posts_edit_access_button" class="ure_toolbar_button" title="<?php echo $button_title; ?>"><?php echo $button_label; ?></button>
<?php
        
    }

    // end of add_toolbar_buttons()

    
    public function dialog_html() {
        
?>
        <div id="ure_posts_edit_access_dialog" class="ure-modal-dialog">
            <div id="ure_posts_edit_access_container">
            </div>    
        </div>
<?php        
        
    }
    // end of dialog_html()


    public function add_js() {
        wp_register_script('ure-posts-edit-access', plugins_url('/js/pro/posts-edit-access.js', URE_PLUGIN_FULL_PATH));
        wp_enqueue_script ('ure-posts-edit-access');
        wp_localize_script('ure-posts-edit-access', 'ure_data_posts_edit_access',
                array(
                    'posts_edit' => esc_html__('Posts Edit', 'user-role-editor'),
                    'dialog_title' => esc_html__('Posts Edit Access', 'user-role-editor'),
                    'update_button' => esc_html__('Update', 'user-role-editor')
                ));
    }
    // end of add_js()    
        
    
    private function get_data_from_post() {
        
        $keys_to_skip = array(
            'action', 
            'ure_nonce', 
            '_wp_http_referer', 
            'ure_object_type', 
            'ure_object_name', 
            'user_role', 
            'ure_access_model',
            'ure_posts_list');
        $access_model = $_POST['ure_access_model'];
        if ($access_model!=1 && $access_model!=2) { // got invalid value
            $access_model = 1;  // use default value
        }        
        $access_error_action = $_POST['ure_post_access_error_action'];
        if ($access_error_action!=1 && $access_error_action!=2) { // got invalid value
            $access_error_action = 1;  // use "return 404 HTTP error" as a default value
        }
        $access_data = array(
            'access_model'=>$access_model, 
            'access_error_action'=>$access_error_action,
            'data'=>array('terms'=>array(), 'posts'=>array()));
        foreach (array_keys($_POST) as $key) {
            if (in_array($key, $keys_to_skip)) {
                continue;
            }
            $value = filter_var($key, FILTER_SANITIZE_STRING);
            $values = explode('_', $value);
            $term_id = $values[1];
            if ($term_id>0) {
                $access_data['data']['terms'][] = $term_id;
            }
        }
        
        if (!empty($_POST['ure_posts_list'])) {
            $posts_list = explode(',', trim($_POST['ure_posts_list']));
            if (count($posts_list)>0) {                
                $access_data['data']['posts'] = $this->lib->filter_int_array($posts_list);
            }            
        }
        
        return $access_data;
    }
    // end of get_data_from_post()
    
    
    private function save_data($role_id) {
        $access_for_role = $this->get_data_from_post();
        $access_data = get_option(self::ACCESS_DATA_KEY);        
        if (!is_array($access_data)) {
            $access_data = array();
        }
        if (count($access_for_role)>0) {
            $access_data[$role_id] = $access_for_role;
        } else {
            unset($access_data[$role_id]);
        }
        update_option(self::ACCESS_DATA_KEY, $access_data);
    }
    // end of save_data()
    
            
    public function update_data() {
    
        if (!isset($_POST['action']) || $_POST['action']!=='ure_update_posts_edit_access') {
            return;
        }
        
        if (!current_user_can(self::edit_posts_access_cap)) {
            $this->lib->notification = esc_html__('URE: you have not enough permissions to use this add-on.', 'user-role-editor');
            return;
        }
        $ure_object_type = filter_input(INPUT_POST, 'ure_object_type', FILTER_SANITIZE_STRING);
        if ($ure_object_type!=='role') {
            $this->lib->notification = esc_html__('URE: posts view access: Wrong object type. Data was not updated.', 'user-role-editor');
            return;
        }
        $ure_object_name = filter_input(INPUT_POST, 'ure_object_name', FILTER_SANITIZE_STRING);
        if (empty($ure_object_name)) {
            $this->lib->notification = esc_html__('URE: posts view access: Empty object name. Data was not updated', 'user-role-editor');
            return;
        }
                        
        if ($ure_object_type=='role') {
            $this->save_access_data($ure_object_name);
        }
        
        $this->lib->notification = esc_html__('Posts edit access data was updated successfully', 'user-role-editor');
    }
    // end of update_data()        

}
// end of URE_Posts_Edit_Access_Role class