<?php

/*
 * User Role Editor WordPress plugin
 * Class URE_Admin_Menu_Access - prohibit selected menu items for role or user
 * Author: Vladimir Garagulya
 * Author email: support@role-editor.com
 * Author URI: https://role-editor.com
 * License: GPL v2+ 
 */

class URE_Admin_Menu_Access {

    const DEBUG = false;
    
    // reference to the code library object
    private $lib = null;    

    public function __construct($lib) {
        
        $this->lib = $lib;
        
        add_action('ure_role_edit_toolbar_service', 'URE_Admin_Menu_View::add_toolbar_buttons');
        add_action('ure_load_js', 'URE_Admin_Menu_View::add_js');
        add_action('ure_dialogs_html', 'URE_Admin_Menu_View::dialog_html');
        add_action('ure_process_user_request', 'URE_Admin_Menu::update_data');
        
        add_action('admin_menu', 'URE_Admin_Menu_Copy::update', 100);
        add_action('admin_head', array($this, 'protect'), 100);
        add_action( 'customize_controls_init', array($this, 'redirect_blocked_urls'), 10);  // Especially for the customize.php URL
        //add_action('wp_head', array($this, 'hide_admin_menu_bar'));
        add_action('wp_before_admin_bar_render', array($this, 'modify_admin_menu_bar'), 99);
        add_filter('media_view_strings', array($this, 'block_media_upload'), 99);
    }
    // end of __construct()

    
    public static function do_not_apply() {
        global $current_user;
        
        $lib = URE_Lib_Pro::get_instance();
        
        if (!$lib->multisite && $lib->user_has_capability($current_user, 'administrator')) {
            return true;
        }
        if ($lib->multisite && is_super_admin() && !$lib->raised_permissions) {
            return true;
        }
        
        return false;
    }
    // end of do_not_apply()


    public function protect() {
        
        $this->redirect_blocked_urls();
        $this->remove_blocked_menu_items();
    }
    // end of protect()
    
    
    // Compare links of the current WordPress admin submenu and its copy used by User Role Editor admin access add-on
    private function is_the_same_submenu_item($key, $key1, $submenu_copy) {
        global $submenu;
        
        if (!isset($submenu_copy[$key][$key1])) {
            return false;
        }
        
        $link1 = URE_Admin_Menu::normalize_link($submenu[$key][$key1][2]);
        $link2 = URE_Admin_Menu::normalize_link($submenu_copy[$key][$key1][2]);
        
        if ($link1!==$link2) { // submenu item does not match with the same index at a copy
            return false;
        }
        
        return true;
    }
    // end of is_the_same_submenu_item()
    
    
    // Check if WordPress admin menu link is included into the menu copy, used by User Role Editor admin access add-on
    private function get_key_from_menu_copy($menu_item, $menu_copy) {
        
        $key_found = false;
        foreach($menu_copy as $key=>$menu_item1) {
            if ($menu_item[2]==$menu_item1[2]) {
                $key_found = $key;
                break;
            }
        }
        
        return $key_found;
    }
    // end of is_the_same_menu_item()
    
        
    private function remove_blocked_menu_items() {
        global $current_user, $menu, $submenu;
                        
        if (self::do_not_apply()) {
            return;
        }        
        
        $blocked = URE_Admin_Menu::load_menu_access_data_for_user($current_user);
        if (empty($blocked)) {
            return;
        }

        $menu_copy = get_option(URE_Admin_Menu::ADMIN_MENU_COPY_KEY);
        $submenu_copy = get_option(URE_Admin_Menu::ADMIN_SUBMENU_COPY_KEY);
        
        foreach($submenu as $key=>$menu_item) {
            foreach($menu_item as $key1=>$menu_item1) {
                if (!$this->is_the_same_submenu_item($key, $key1, $submenu_copy)) {
                    continue;
                }                
                $link = URE_Admin_Menu::normalize_link($submenu_copy[$key][$key1][3]);
                $item_id = URE_Admin_Menu::calc_menu_item_id('submenu', $link);
                if ( ($blocked['access_model']==1 && in_array($item_id, $blocked['data'])) ||
                     ($blocked['access_model']==2 && !in_array($item_id, $blocked['data'])) ) {
                    unset($submenu[$key][$key1]);
                }
            }    
        }        

        if (self::DEBUG) {
            echo '<!--'. PHP_EOL;
            echo 'URE - debug:'. PHP_EOL;
            echo '------------'. PHP_EOL;
        }
        foreach($menu as $key=>$menu_item) {
            if (self::DEBUG) {
                echo $menu[$key][2];
            }    
            $key1 = $this->get_key_from_menu_copy($menu_item, $menu_copy);
            if ($key1===false) { // menu item does not found at menu copy
                if (self::DEBUG) {
                    echo ' - skipped'. PHP_EOL;
                }
                continue;
            }                        
            
            $link = URE_Admin_Menu::normalize_link($menu_copy[$key1][3]);
            $item_id1 = URE_Admin_Menu::calc_menu_item_id('menu', $link);
            $item_id2 = URE_Admin_Menu::calc_menu_item_id('submenu', $link);
            if ($blocked['access_model']==1) {
                if (in_array($item_id1, $blocked['data']) ||
                    (in_array($item_id2, $blocked['data']) && 
                     (!isset($submenu[$menu_item[2]]) || count($submenu[$menu_item[2]])==0))) {
                    unset($submenu[$menu_item[2]]);
                    unset($menu[$key]);
                    if (self::DEBUG) {
                        echo ' - blocked';
                    }
                }
            } elseif ($blocked['access_model']==2) {
                if (!in_array($item_id1, $blocked['data']) && !in_array($item_id2, $blocked['data'])) {                    
                    unset($submenu[$menu_item[2]]);
                    unset($menu[$key]);
                    if (self::DEBUG) {
                        echo ' - blocked';
                    }
                }
            }
            if (self::DEBUG) {
                echo PHP_EOL;
            }
        }
        if (self::DEBUG) {
            echo PHP_EOL .'-->'. PHP_EOL;
        }
    }
    // end of remove_blocked_menu_items()
    
        
    protected function extract_command_from_url($url) {
        
        $path = parse_url($url, PHP_URL_PATH);
        $path_parts = explode('/', $path);
        $url_script = end($path_parts);
        $url_query = parse_url($url, PHP_URL_QUERY);
        
        $command = $url_script;
        if (!empty($url_query)) {
            $command .= '?'. $url_query;
        }
        $command = str_replace('&', '&amp;', $command);
        if (empty($command)) {
            $command = 'index.php';
        }
        
        return $command;
        
    }
    // end of extract_command_from_url()
    
    
    private function get_first_available_menu_item() {
    
        global $menu;
        
        $menu_copy = get_option(URE_Admin_Menu::ADMIN_MENU_COPY_KEY);
        $available = '';
        foreach ($menu as $key=>$menu_item) {
            if ($menu_item[4]==='wp-menu-separator' || $menu_item[4]==='separator-woocommerce') {
                continue;
            }
            $key1 = $this->get_key_from_menu_copy($menu_item, $menu_copy);
            if ($key1===false) { // menu item does not found at menu copy
                continue;
            }
            
            $available = get_option('siteurl') .'/wp-admin/'. $menu_copy[$key1][3];
            break;
            
        }

        if (empty($available)) {
            $available = get_option('siteurl') .'/wp-admin/index.php';
        }
        
        return $available;
        
    }
    // end of get_first_available_menu()
    
    
    /*
     * remove Welcome panel from the dashboard as 
     * it's not good to show direct links to WordPress functionality for restricted user
     */ 
    private function remove_welcome_panel($command, $blocked_data, $access_model) {
        if ($command!=='index.php') { 
            return;
        }
        
        $customize_hash = '71cf5c9f472f8adbfc847a3f71ce9f0e'; /* 'submenu'.'customize.php' */
        if (($access_model==1 && in_array($customize_hash, $blocked_data)) || 
            ($access_model==2 && !in_array($customize_hash, $blocked_data))) {
            remove_action('welcome_panel', 'wp_welcome_panel'); 
        }
        
    }
    // end of remove_welcome_panel()
    
    
    private function command_from_main_menu($command) {
        $result = false;
        $menu_hashes = URE_Admin_Menu::get_menu_hashes();
        $command_list = array_keys($menu_hashes);
        foreach($command_list as $menu_link) {
            if ($menu_link===$command || strpos($menu_link, $command)!==false || strpos($command, $menu_link)!==false) {
                $result = true;
                break;
            }
        }
        
        return $result;
    }
    // end of command_from_main_menu()
    
    
    public function redirect_blocked_urls() {
        
        global $current_user;
        
        if (self::do_not_apply()) {
            return;
        }        
        
        $url = strtolower($_SERVER['REQUEST_URI']);
        $command = $this->extract_command_from_url($url);
        $item_id1 = URE_Admin_Menu::calc_menu_item_id('menu', $command);
        $item_id2 = URE_Admin_Menu::calc_menu_item_id('submenu', $command);
        $blocked = URE_Admin_Menu::load_menu_access_data_for_user($current_user);
        if ($blocked['access_model']==1 && 
            !in_array($item_id1, $blocked['data']) && 
            !in_array($item_id2, $blocked['data'])) {
            $this->remove_welcome_panel($command, $blocked['data'], 1);
            return;
        }
        if ($blocked['access_model']==2) {
            if (in_array($item_id1, $blocked['data']) || 
                in_array($item_id2, $blocked['data']))  { 
                $this->remove_welcome_panel($command, $blocked['data'], 2);
                return;
            }            
            // if command was not selected but it does not match with any admin menu (submenu) item - do not block it
            if (!$this->command_from_main_menu($command)) {
                return;
            }
        }
                
        $url = $this->get_first_available_menu_item();
        if (headers_sent()) {
?>
<script>
    document.location.href = '<?php echo $url; ?>';
</script>    
<?php
            die;
        } else {
            wp_redirect($url);
        }
        
    }
    // end of redirect_blocked_urls()

    
    /**
     * Hide WordPress admin menu bar for user with blocked WordPress admin menu items
     * @TODO: It is better to remove blocked menu items from the front end admin menu bar.
     * 
     * @global WP_User $current_user
     * @return void
     */
    public function hide_admin_menu_bar() {
        global $current_user;
        
        if (self::do_not_apply()) {
            return;
        }        
        
        $blocked = URE_Admin_Menu::load_menu_access_data_for_user($current_user);
        if (!empty($blocked)) {
            show_admin_bar(false);
        }
        
    }
    // end of hide_admin_menu_bar()

    
    /**
     * Return admin menu bar command string, 
     * but false for admin bar menu items which should be ignored
     * 
     * @param object $menu_item
     * @return boolean
     */
    protected function get_admin_menu_bar_command($menu_item) {
        
        $ignore_list = array(
            'about.php',
            'index.php',
            'profile.php',
            'wp-login.php'
        );
        if (empty($menu_item->href)) {
            return false;
        }
        $command = $this->extract_command_from_url($menu_item->href);
        foreach($ignore_list as $skip_it) {
            if (strpos($command, $skip_it)!==false) {
                return false;
            }
        }
                
        return $command;
    }
    // end of get_admin_menu_bar_command()
    
    
    /**
     * For front-end only
     * 
     * @global WP_User $current_user
     * @global type $wp_admin_bar
     * @return void
     */
    public function modify_admin_menu_bar() {
        global $current_user, $wp_admin_bar;
                
        $nodes = $wp_admin_bar->get_nodes();
        if (empty($nodes)) {
            return;
        }
        
        if (self::do_not_apply()) {
            return;
        }        
        
        // remove 'SEO' menu from top bar
        if (!current_user_can('manage_options')) {
            $wp_admin_bar->remove_menu('wpseo-menu');
        } 
        
        $blocked = URE_Admin_Menu::load_menu_access_data_for_user($current_user);
        if (empty($blocked)) {
            return;
        }                
        
        // if 'SEO' menu is blocked for the role, block it at top bar
        $seo_item_id = 'e960550080acc7b8154fddae02b72542';        // 'menu'.'admin.php?page=wpseo_dashboard'
        if ( ($blocked['access_model']==1 && in_array($seo_item_id, $blocked['data'])) ||
             ($blocked['access_model']==2 && !in_array($seo_item_id, $blocked['data'])) ) {
            $wp_admin_bar->remove_menu('wpseo-menu');
        }
        
        foreach($nodes as $key=>$menu_item) {
            $command = $this->get_admin_menu_bar_command($menu_item);
            if (empty($command)) {
                continue;
            }
            
            $item_id1 = URE_Admin_Menu::calc_menu_item_id('menu', $command);
            $item_id2 = URE_Admin_Menu::calc_menu_item_id('submenu', $command);
            
            if ($blocked['access_model']==1) {  // block selected
                if (in_array($item_id1, $blocked['data'])) {
                    $wp_admin_bar->remove_menu($menu_item->id);
                } elseif (in_array($item_id2, $blocked['data'])) {
                    $wp_admin_bar->remove_node($menu_item->id);
                }
            } elseif ($blocked['access_model']==2) {    // block not selected
                if (!in_array($item_id1, $blocked['data']) && !in_array($item_id2, $blocked['data'])) {
                    $wp_admin_bar->remove_menu($menu_item->id);                
                }
            }
        }
                
    }
    // end of modify_admin_menu_bar()
    
    
    public function block_media_upload($strings) {
        
        global $current_user;
        
        if (self::do_not_apply()) {
            return $strings;
        }

        $blocked = URE_Admin_Menu::load_menu_access_data_for_user($current_user);
        if (empty($blocked)) {
            return $strings;
        }
        
        foreach($blocked['data'] as $menu_hash) {
            if ($menu_hash=='a6d96d2991e9d58c1d04ef3c2626da56') {  // Media -> Add New
                // Undocumented trick to remove "Upload Files" tab at the Post Editor "Add Media" popup window 
                // Be aware - it may stop working with next version of WordPress
                unset($strings['uploadFilesTitle']);    
                break;
            }
        }
                        
        return $strings;
    }
    // end of block_media_upload()
}
// end of URE_Admin_Menu_Access class
