<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
	<aside class="widget-area m-pull-9 m3" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside>