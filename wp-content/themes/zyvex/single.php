<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="main" role="main">
		<div class="row">
			<div class="m12">
				<?php
					while ( have_posts() ) {
						the_post();
						get_template_part( 'content', get_post_type() );
						// comments_template( '', true );
					}
				?>
				<div class="row">
					<div class="s6 prev-post">
						<?php previous_post_link(); ?>
						&nbsp;
					</div>

					<div class="s6 next-post">
						&nbsp;
						<?php next_post_link(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>