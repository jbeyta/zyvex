<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main" role="main">
		<div class="row">
			<?php
				// wether or not we're showing the overview link
				$showverview = false;

				// if this current page is a grandchild, assume no
				$is_grandchild = false;

				// default size
				$size = 'm9';

				// get the ancestors of this page
				$ancestors = get_post_ancestors($post->ID);

				// how many ancestors
				$count_anc = count($ancestors);

				// current page class
				$current = '';

				// empty so we can put in the parents
				$child_of = '';

				// wether or not to show the sidenav assume yes
				$showsidenav = true;

				$parent = get_post($post->post_parent);

				// check if we're on a child page and then list out all siblings
				if($post->post_parent) {
					$child_of = $post->ID;
					$current = 'current_page_item';

					if(count($ancestors) > 1) {
						$child_of = $post->post_parent;

						$current = '';
					}

					if($parent->post_parent) {
						$is_grandchild = true;
					}

					if(count($ancestors) > 2) {
						$child_of = $parent->post_parent;

						$current = '';
					}
				}

				$childs = get_children(array('post_type' => 'page', 'post_parent' => $post->ID));

				if(empty($childs) && $count_anc < 2) {
					$showsidenav = false;
				}

				if(!empty($childs)) {
					$showverview = true;
				}				

				if($is_grandchild) {
					$showverview = true;
				}

				if(is_page(176) || $post->post_parent == 176) {
					$showverview = true;
					$showsidenav = true;
					$child_of = 20;
				}

				if(!empty($child_of) && $showsidenav) {
					echo '<aside class="widget-area m3" role="complementary">';
						echo '<ul class="styleless sidenav child-page">';
							if($showverview) {
								if(is_page(176) || $post->post_parent == 176) {
									if(is_page(176)) {
										$current = 'current_page_item';
									} else {
										$current = '';
									}
									echo '<li class="page_item page-item-'.$child_of.' '.$current.'"><a href="'.get_the_permalink(176).'">Overview</a></li>';
								} else {
									echo '<li class="page_item page-item-'.$child_of.' '.$current.'"><a href="'.get_the_permalink($child_of).'">Overview</a></li>';
								}
							}

							if(is_page(176) || $post->post_parent == 176) {
								$child_of = 176;
							}

							wp_list_pages(array(
								'title_li' => '',
								'child_of' => $child_of,
							));
						echo '</ul>';
						// if we're on the parent page and then check if the page has children
					echo '</aside>';
				} else {
					$size = 'm12';
				}

				$usePageSects = get_post_meta($post->ID, '_cwmb_page_sections_sidenav', true);
				$pageSectsContentClass = '';
				$pageSectsContentId = '';
				if(!empty($usePageSects) && $usePageSects == 'on') {
					echo '<aside class="widget-area m3" role="complementary">';
					echo '<ul class="styleless sidenav child-page page-sections-nav">';
					echo '</ul>';
					echo '</aside>';
					echo '<a href="#overview" class="page-sect-item back-to-top"><i class="fa fa-chevron-down fa-lg" aria-hidden="true"></i></a>';

					$pageSectsContentClass = 'page-sections-content';
					$pageSectsContentId = 'overview';
					$size = 'm9';
					echo '<div class="page-sect-item back-to-top"><a href="#overview"><i class="fa fa-chevron-up fa-lg" aria-hidden="true"></i></a></div>';
				}
			?>

			<div id="<?php echo $pageSectsContentId; ?>" class="<?php echo $size; ?> <?php echo $pageSectsContentClass; ?>">
				<?php
					if(have_posts()) {
						while(have_posts()) {
							the_post();

							the_content();
							// comments_template( '', true );
						}
					}
				?>
			</div>
		</div>

	</div>

<?php get_footer(); ?>