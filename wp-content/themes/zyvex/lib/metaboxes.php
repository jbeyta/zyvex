<?php
// ------------------------------------
//
// Custom Meta Boxes
//
// ------------------------------------
// custom meta boxes
function cw_list_services() {
	if(!is_admin()) {
		return;
	}

	$sargs = array(
		'post_type' => 'services',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	global $cw_services_list;
	global $cw_services_ids;
	$cw_services_list = array();
	$cw_services_ids = array();

	$servs = new WP_Query($sargs);
	if($servs->have_posts()) {
		while($servs->have_posts()) {
			$servs->the_post();
			global $post;
			$cw_services_list[$post->ID] = get_the_title();
			array_push($cw_services_ids, $post->ID);
		}
	}
}
add_action('init', 'cw_list_services');

function list_pages() {
	if(!is_admin()) {
		return;
	}
	global $post;

	$pages_args = array(
		'post_type' => 'page',
		'order' => 'ASC',
		'orderby' => 'title',
		'posts_per_page' => -1
	);

	$pages = new WP_Query($pages_args);

	global $cw_page_list;
	$cw_page_list = array('' => 'Select a page');

	if($pages->have_posts()) {
		while($pages->have_posts()) {
			$pages->the_post();
			
			$cw_page_list[$post->ID] = get_the_title();
		}
	}
	wp_reset_query();
}
add_action('init', 'list_pages');

function cw_list_media() {
	if(!is_admin()) {
		return;
	}
	
	$margs = array(
		'post_type' => 'attachment',
		'post_mime_type' =>'application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'post_status' => 'inherit',
		'posts_per_page' => -1
	);

	$media = new WP_Query( $margs );

	global $cw_media_list;
	$cw_media_list = array('' => 'Select a file');
	foreach($media->posts as $file) {
		$cw_media_list[$file->ID] = $file->post_title;
	}
}
add_action('init', 'cw_list_media');

function cw_metaboxes( array $meta_boxes ) {
	global $cw_page_list;
	global $cw_services_list;
	global $cw_services_ids;
	// use for select, checkbox, radio of list of states
	global $cw_states;

	$prefix = '_cwmb_'; // Prefix for all fields

	// $meta_boxes['slide_video'] = array(
	// 	'id' => 'slide_video',
	// 	'title' => 'Slide Video Files',
	// 	'object_types' => array( 'slides' ), // Post type
	// 	'context' => 'normal',
	// 	'priority' => 'high',
	// 	'show_names' => true, // Show field names on the left
	// 	'fields' => array(
	// 		array(
	// 			'name' => 'Video mp4',
	// 			'desc' => 'NOTE: Uploading video will override any other content. Both video codec must be uploaded for video to play.',
	// 			'id' => $prefix.'slide_mp4',
	// 			'type' => 'file',
	// 		),
	// 		array(
	// 			'name' => 'Video webm',
	// 			'desc' => 'NOTE: Uploading video will override any other content. Both video codec must be uploaded for video to play.',
	// 			'id' => $prefix.'slide_webm',
	// 			'type' => 'file',
	// 		)
	// 	)
	// );

	$slides = new_cmb2_box( array(
		'id'            => $prefix.'slides',
		'title'         => 'Slide Info',
		'object_types'  => array( 'slides', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$slides->add_field( array(
		'name' => 'Title',
		'id' => $prefix.'slide_title',
		'type' => 'text',
	) );

	// $slides->add_field( array(
	// 	'name' => 'Subtitle',
	// 	'id' => $prefix.'slide_subtitle',
	// 	'type' => 'text',
	// ) );

	// $slides->add_field( array(
	// 	'name' => 'Title Position',
	// 	'id' => $prefix.'slide_title_pos',
	// 	'type' => 'radio',
	// 	'options' => array(
	// 		'left' => 'Left',
	// 		'center' => 'Center',
	// 		'right' => 'Right'
	// 	),
	// 	'default' => 'left'
	// ) );

	$slides->add_field( array(
		'name' => 'Image',
		'id' => $prefix.'slide_image',
		'type' => 'file'
	) );

	// $slides->add_field( array(
	// 	'name' => 'Image Position',
	// 	'id' => $prefix.'slide_image_pos',
	// 	'type' => 'radio',
	// 	'options' => array(
	// 		'left' => 'Left',
	// 		'center' => 'Center',
	// 		'right' => 'Right'
	// 	),
	// 	'default' => 'center'
	// ) );

	$slides->add_field( array(
		'name' => 'Caption',
		'id' => $prefix.'slide_caption',
		'type' => 'textarea'
	) );

	// $slides->add_field( array(
	// 	'name' => 'Caption Position',
	// 	'id' => $prefix.'slide_caption_pos',
	// 	'type' => 'radio',
	// 	'options' => array(
	// 		'left' => 'Left',
	// 		'center' => 'Center',
	// 		'right' => 'Right'
	// 	),
	// 	'default' => 'left'
	// ) );

	$slides->add_field( array(
		'name' => 'Link',
		'id' => $prefix.'slide_link',
		'type' => 'text_url'
	) );

	// $slides->add_field( array(
	// 	'name' => 'Background Color',
	// 	'id' => $prefix.'slide_color',
	// 	'type' => 'radio',
	// 	'options' => array (
	// 		'#ffffff' => 'None',
	// 		'#0075bf' => 'Blue',
	// 		'#ff685a' => 'Salmon',
	// 		'#5b5b5b' => 'Grey',
	// 		'#4a9231' => 'Green'
	// 	),
	// 	'default' => '#0075bf'
	// ) );

	// $slide_page = new_cmb2_box( array(
	// 	'id'            => $prefix.'slide_page',
	// 	'title'         => 'Slide Page',
	// 	'object_types'  => array( 'slides', ), // Post type
	// 	'context' => 'side',
	// 	'priority' => 'high',
	// 	'show_names'    => true, // Show field names on the left
	// ) );

	// $slide_page->add_field( array(
	// 	'name' => '',
	// 	'desc' => 'Choose page(s) to show this slide on. NOTE: If no page is selected, this slide will note be shown anywhere on the site.',
	// 	'id' => $prefix.'slide_page',
	// 	'type' => 'select',
	// 	'options' => $cw_page_list
	// ) );


	// locations
	$location = new_cmb2_box( array(
		'id'            => $prefix.'locations',
		'title' => 'Location Info',
		'object_types'  => array( 'locations', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$location->add_field( array(
		'name' => 'Address 1',
		'id' => $prefix.'loc_address1',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Address 2',
		'id' => $prefix.'loc_address2',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'City',
		'id' => $prefix.'loc_city',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'State',
		'id' => $prefix.'loc_state',
		'type' => 'select',
		'options' => $cw_states
	) );

	$location->add_field( array(
		'name' => 'Zip',
		'id' => $prefix.'loc_zip',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Phone',
		'id' => $prefix.'loc_phone',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Phone 2',
		'id' => $prefix.'loc_phone2',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Fax',
		'id' => $prefix.'loc_fax',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Email',
		'id' => $prefix.'loc_email',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Photo',
		'id' => $prefix.'loc_photo',
		'type' => 'file'
	) );

	$location->add_field( array(
		'name' => 'Hours',
		'id' => $prefix.'loc_hours',
		'type' => 'textarea'
	) );

	$location->add_field( array(
		'name' => 'Lat',
		'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon',
		'id' => $prefix.'loc_lat',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Lon',
		'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon',
		'id' => $prefix.'loc_lon',
		'type' => 'text'
	) );

	// promos
	$promos = new_cmb2_box( array(
		'id'            => $prefix.'promo',
		'title'         => 'Promo Info',
		'object_types'  => array( 'promos', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$promos->add_field( array(
		'name' => 'Promo Image',
		'id' => $prefix.'promo_image',
		'type' => 'file'
	) );

	$promos->add_field( array(
		'name' => 'Link',
		'id' => $prefix.'promo_link',
		'type' => 'text_url'
	) );

	// services
	$services = new_cmb2_box( array(
		'id'            => $prefix.'service_excerpt',
		'title' => 'Options',
		'object_types'  => array( 'services', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$services->add_field( array(
		'name' => 'Service Image',
		'id' => $prefix.'service_image',
		'type' => 'file'
	) );

	// testimonials
	$testimonials = new_cmb2_box( array(
		'id'            => $prefix.'testimonials',
		'title' => 'Testimonial',
		'object_types'  => array( 'testimonials', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$testimonials->add_field( array(
		'id' => $prefix.'testimonial',
		'type' => 'textarea'
	) );
	$testimonials->add_field( array(
		'name' => 'Vocation',
		'id' => $prefix.'vocation',
		'type' => 'text'
	) );
	$testimonials->add_field( array(
		'name' => 'Location',
		'id' => $prefix.'location',
		'type' => 'text'
	) );

	// staff
	$staff = new_cmb2_box( array(
		'id'            => $prefix.'staff',
		'title' => 'Testimonial',
		'object_types'  => array( 'staff', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$staff->add_field( array(
		'name' => 'Title',
		'id' => $prefix.'staff_title',
		'type' => 'text'
	) );
	$staff->add_field( array(
		'name' => 'Image',
		'id' => $prefix.'staff_image',
		'type' => 'file'
	) );

	$staff->add_field( array(
		'name' => 'Tab 2 title',
		'id' => $prefix.'staff_tab2_title',
		'type' => 'text'
	) );
	$staff->add_field( array(
		'name' => 'Tab 2 Text',
		'id' => $prefix.'staff_tab2',
		'type' => 'wysiwyg',
		'after_row' => '<hr style="margin-bottom: 30px;">'
	) );

	$staff->add_field( array(
		'name' => 'Tab 3 title',
		'id' => $prefix.'staff_tab3_title',
		'type' => 'text'
	) );
	$staff->add_field( array(
		'name' => 'Tab 3 Text',
		'id' => $prefix.'staff_tab3',
		'type' => 'wysiwyg',
		'after_row' => '<hr style="margin-bottom: 30px;">'
	) );

	$staff->add_field( array(
		'name' => 'Tab 4 title',
		'id' => $prefix.'staff_tab4_title',
		'type' => 'text'
	) );
	$staff->add_field( array(
		'name' => 'Tab 4 Text',
		'id' => $prefix.'staff_tab4',
		'type' => 'wysiwyg',
		'after_row' => '<hr style="margin-bottom: 30px;">'
	) );

	$staff->add_field( array(
		'name' => 'Tab 5 title',
		'id' => $prefix.'staff_tab5_title',
		'type' => 'text'
	) );
	$staff->add_field( array(
		'name' => 'Tab 5 Text',
		'id' => $prefix.'staff_tab5',
		'type' => 'wysiwyg',
		'after_row' => '<hr style="margin-bottom: 30px;">'
	) );

	$staff->add_field( array(
		'name' => 'Tab 6 title',
		'id' => $prefix.'staff_tab6_title',
		'type' => 'text'
	) );
	$staff->add_field( array(
		'name' => 'Tab 6 Text',
		'id' => $prefix.'staff_tab6',
		'type' => 'wysiwyg',
		'after_row' => '<hr style="margin-bottom: 30px;">'
	) );

	$staff->add_field( array(
		'name' => 'Tab 7 title',
		'id' => $prefix.'staff_tab7_title',
		'type' => 'text'
	) );
	$staff->add_field( array(
		'name' => 'Tab 7 Text',
		'id' => $prefix.'staff_tab7',
		'type' => 'wysiwyg',
		'after_row' => '<hr style="margin-bottom: 30px;">'
	) );

	// galleries
	$galleries = new_cmb2_box( array(
		'id'            => $prefix.'galleries',
		'title' => 'Gallery Info',
		'object_types'  => array( 'galleries', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$galleries->add_field( array(
		'name' => 'Images',
		'id' => $prefix.'gallery_images',
		'type' => 'file_list'
	) );

	$galleries->add_field( array(
		'name' => 'Description',
		'id' => $prefix.'gallery_desc',
		'type' => 'textarea'
	) );

	// page_subtitle
	$page_subtutle = new_cmb2_box( array(
		'id'            => $prefix.'page_subtutle',
		'title' => 'Subtitle',
		'object_types'  => array( 'page', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => false, // Show field names on the left
	) );

	$page_subtutle->add_field( array(
		'name' => 'Subtitle',
		'id' => $prefix.'subtitle',
		'type' => 'textarea'
	) );

	// homepage_bg
	$homepage_bg = new_cmb2_box( array(
		'id'            => $prefix.'homepage_bg',
		'title' => 'Background Image',
		'object_types'  => array( 'page', ), // Post type
		// 'show_on' => array('key' => 'page-template', 'value' => 'front-page.php'),
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => false, // Show field names on the left
	) );

	$homepage_bg->add_field( array(
		'name' => 'Image',
		'id' => $prefix.'bg_img',
		'type' => 'file'
	) );

	// page_sections_nav
	$page_sections_nav = new_cmb2_box( array(
		'id'            => $prefix.'page_sections_nav',
		'title' => 'Page Sections',
		'object_types'  => array( 'page', ), // Post type
		// 'show_on' => array('key' => 'page-template', 'value' => 'front-page.php'),
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$page_sections_nav->add_field( array(
		'name' => 'NOTE: Used with page section shortcode<br><br>[page_section id="content" size="h4"] content [/page_section]<br><br> id is required and must be unique, size is the header tag, so valid header tags only',
		'desc' => 'Use Page Sections as Sidebar Navigation',
		'id' => $prefix.'page_sections_sidenav',
		'type' => 'checkbox'
	) );

	// contests
	$contests = new_cmb2_box( array(
		'id'            => $prefix.'contests',
		'title' => 'Contest Logo',
		'object_types'  => array( 'contests', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => false, // Show field names on the left
	) );

	$contests->add_field( array(
		'name' => 'Contest Logo',
		'id' => $prefix.'contest_logo',
		'type' => 'file'
	) );

	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'cw_metaboxes' );

// end custom meta boxes