<?php
class cw_sidenav extends WP_Widget {

function __construct() {
	parent::__construct(
		// Base ID of your widget
		'cw_sidenav', 

		// Widget name will appear in UI
		__('CW Sub-pages Side Nav', 'cw_sidenav_domain'),

		// Widget description
		array( 'description' => __( 'Creates navigation for sub-pages and their children.', 'cw_sidenav_domain' ), ) 
	);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
	global $post;
	$title = apply_filters( 'widget_title', $instance['title'] );

	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

	// This is where you run the code and display the output

	get_template_part('content', 'subpage-sidenav');

	echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	} else {
		$title = __( '', 'cw_sidenav_domain' );
	}
// Widget admin form
?>

<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>

<?php }

			// Updating widget replacing old instances with new
			public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			return $instance;
		}
	} // Class cw_sidenav ends here

// Register and load the widget
function cw_sidenav_load_widget() {
	register_widget( 'cw_sidenav' );
}
add_action( 'widgets_init', 'cw_sidenav_load_widget' );