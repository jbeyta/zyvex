<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div role="main" class="search main-content">
	<div class="page-header-variant">
		<div class="row">
			<div class="s12">
				<?php
					if ( have_posts() && strlen( trim(get_search_query()) ) != 0) {
						echo '<h2 class="page-title">Results for: &ldquo;'.get_search_query().'&rdquo;</h2>';

						while ( have_posts() ) {
							the_post();
							get_template_part( 'content', 'search' );
						}
					} elseif(have_posts() && strlen( trim(get_search_query()) ) == 0) {
						echo '<h2 class="page-title">Oops! Nothing was put into the search field.</h2>';
					} else {
						echo '<h2 class="page-title">Nothing found for: &ldquo;'.get_search_query().'&rdquo;</h2>';
					}
				?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="m12">
			<?php
				if ( have_posts() && strlen( trim(get_search_query()) ) != 0) {
					while ( have_posts() ) {
						the_post();
						get_template_part( 'content', 'search' );
					}
				}
			?>
		</div>
	</div>
</div>
<?php get_footer();