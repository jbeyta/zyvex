<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header();
$tab2 = get_post_meta($post->ID, '_cwmb_staff_tab2', true);
$tab3 = get_post_meta($post->ID, '_cwmb_staff_tab3', true);
$tab4 = get_post_meta($post->ID, '_cwmb_staff_tab4', true);
$tab5 = get_post_meta($post->ID, '_cwmb_staff_tab5', true);
$tab6 = get_post_meta($post->ID, '_cwmb_staff_tab6', true);
$tab7 = get_post_meta($post->ID, '_cwmb_staff_tab7', true);

$tabs_content = '';
$tab_key = 1;

if(!empty($tab2)) {
	$tabs_content .= '<div class="tab-content" id="tab-'.$tab_key.'">'.apply_filters('the_content', $tab2).'</div>';
	$tab_key++;
}
if(!empty($tab3)) {
	$tabs_content .= '<div class="tab-content" id="tab-'.$tab_key.'">'.apply_filters('the_content', $tab3).'</div>';
	$tab_key++;
}
if(!empty($tab4)) {
	$tabs_content .= '<div class="tab-content" id="tab-'.$tab_key.'">'.apply_filters('the_content', $tab4).'</div>';
	$tab_key++;
}
if(!empty($tab5)) {
	$tabs_content .= '<div class="tab-content" id="tab-'.$tab_key.'">'.apply_filters('the_content', $tab5).'</div>';
	$tab_key++;
}
if(!empty($tab6)) {
	$tabs_content .= '<div class="tab-content" id="tab-'.$tab_key.'">'.apply_filters('the_content', $tab6).'</div>';
	$tab_key++;
}
if(!empty($tab7)) {
	$tabs_content .= '<div class="tab-content" id="tab-'.$tab_key.'">'.apply_filters('the_content', $tab7).'</div>';
	$tab_key++;
}

?>
	<div class="main" role="main">
		<div class="row">
			<div class="s12 cwtabs-container">
				<?php
					while ( have_posts() ) {
						the_post();
						$content = get_the_content();
					}

					if(!empty($tabs_content)) {
						echo '<div id="tab-0" class="tab-content">'.apply_filters('the_content', $content).'</div>';
						echo $tabs_content;
					} else {
						echo apply_filters('the_content', $content);
					}
				?>
			</div>
			<div class="s12">
				<a class="button v2" style="margin-top: 30px;" href="/meet-the-team/">&lt; Meet the Team page</a>
			</div>
		</div>
	</div>

<?php get_footer(); ?>