<?php
/**
 * Template Name: Home Page Template
 * Description: Custom home page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	
	<div role="main">
		<div class="row">
			<div class="s12">
				<?php
					echo '<div class="content-container">';
						$page_subtitle = get_post_meta($page_id, '_cwmb_subtitle', true);
						if(!empty($page_subtitle)) {
							echo '<h2 class="home-pagetitle">'.nl2br($page_subtitle).'</h2>';
						}
						the_content();
					echo '</div>';
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>