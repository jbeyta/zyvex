<?php
	// if(is_404() || is_search())
	// 	return;

	// global $post;
	// $page_id = $post->ID;

	$slides_args = array(
		'post_type' => 'slides',
		'posts_per_page' => -1,
		// 'meta_key' => '_cwmb_slide_page',
		// 'meta_value' => $page_id,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	$the_slides = new WP_Query($slides_args);

	// echo_pre($page_id);

	if($the_slides->have_posts()){
		$mode = cw_slideshow_options_get_option('_cwso_mode');
		$controls = cw_slideshow_options_get_option('_cwso_controls');
		$pager = cw_slideshow_options_get_option('_cwso_pager');
		$speed = cw_slideshow_options_get_option('_cwso_speed');
		$pause = cw_slideshow_options_get_option('_cwso_pause');

		if(empty($mode)) {
			$mode = 'horizontal';
		}

		if(empty($controls)) {
			$controls = 'true';
		}

		if(empty($pager)) {
			$pager = 'false';
		}

		if(empty($speed)) {
			$speed = '500';
		} else {
			$speed = $speed*1000;
		}

		if(empty($pause)) {
			$pause = '5000';
		} else {
			$pause = $pause*1000;
		}

		echo '<div class="cw-slideshow">';
			echo '<ul class="styleless cw-slider" data-mode="'.$mode.'" data-controls="'.$controls.'" data-pager="'.$pager.'" data-speed="'.$speed.'" data-pause="'.$pause.'">';
			while($the_slides->have_posts()) {
				$the_slides->the_post();

				$slide_title = get_post_meta($post->ID, '_cwmb_slide_title', true);
				// $slide_subtitle = get_post_meta($post->ID, '_cwmb_slide_subtitle', true);
				// $slide_title_pos = get_post_meta($post->ID, '_cwmb_slide_title_pos', true);
				$slide_image = get_post_meta($post->ID, '_cwmb_slide_image', true);
				// $slide_image_pos = get_post_meta($post->ID, '_cwmb_slide_image_pos', true);
				$slide_caption = get_post_meta($post->ID, '_cwmb_slide_caption', true);
				// $slide_caption_pos = get_post_meta($post->ID, '_cwmb_slide_caption_pos', true);
				$slide_link = get_post_meta($post->ID, '_cwmb_slide_link', true);
				// $slide_color = get_post_meta($post->ID, '_cwmb_slide_color', true);

				// $slide_mp4 = get_post_meta($post->ID, '_cwmb_slide_mp4', true);
				// $slide_webm = get_post_meta($post->ID, '_cwmb_slide_webm', true);

				// $video = false;
				$class = 'slide';
				// if(!empty($slide_webm) && !empty($slide_mp4)) {
					// $video = true;
					// $class .= ' show-for-medium-up';
				// }

				$cropped = aq_resize( $slide_image, 1200, 450, true, true, true );

				// if(empty($slide_color)) {
					// $slide_color = '#0075bf';
				// }

				echo '<li class="'.$class.'" data-bgcolor="'.$slide_color.'"><div class="inner align-'.$slide_image_pos.'">';
					if(!empty($slide_link)) { echo '<a href="'.$slide_link.'">'; }

						// if($video == true) {
							// echo '<video id="video" width="1000" height="450" preload="auto" autoplay loop>';
								// echo '<source src="'.$slide_mp4.'" type="video/mp4">';
								// echo '<source src="'.$slide_webm.'" type="video/webm">';
								// echo 'Your browser does not support HTML5 video.';
							// echo '</video>';
						// } else {
							if(!empty($slide_image)) { echo '<img src="'.$cropped.'" alt="" />'; }
							$class = '';
							if(!empty($slide_title) || !empty($slide_caption)) {
								$class = 'has-cap';
							}
							echo '<div class="slide-words '.$class.'">';
								if(!empty($slide_title)) {
									echo '<h3 class="slide-title align-'.$slide_title_pos.'">'.$slide_title;
										// if(!empty($slide_subtitle)) { echo '<br><span class="subtitle">'.$slide_subtitle.'</span>'; }
									echo '</h3>';
								}

								if(!empty($slide_caption)) { echo '<p class="slide-caption align-'.$slide_caption_pos.'">'.nl2br($slide_caption).'</p>'; }
							echo '</div>';
						// }

					if(!empty($slide_link)) { echo '</a>'; }
				echo '</div></li>';
			}
		echo '</ul>';
	echo '</div>'; // cw-slideshow
	}
wp_reset_query(); ?>
