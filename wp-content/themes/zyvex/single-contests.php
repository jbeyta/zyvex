<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main" role="main">
		<div class="row">
			<div class="s12">
				<?php
					if(have_posts()) {
						while(have_posts()) {
							the_post();

							$logo = get_post_meta($post->ID, '_cwmb_contest_logo', true);

							if(!empty($logo)) {
								echo '<img class="aligncenter" src="'.$logo.'" alt="" />';
							}

							the_content();
						}
					}
				?>
			</div>
		</div>

	</div>

<?php get_footer(); ?>