<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
	// echo_pre(get_post_meta($post->ID));
	echo '<div class="staff-content s12 m4">';
		$title = get_post_meta($post->ID, '_cwmb_staff_title', true);
		$image = get_post_meta($post->ID, '_cwmb_staff_image', true);
		

		if(!empty($image)) {
			$cropped = aq_resize($image, 640, 430, true, true, true);

			if(empty($cropped)) {
				$cropped = $image;
			}

			echo '<a href="'.get_the_permalink().'"><img class="staff-image" src="'.$cropped.'" alt="" /></a>';
		}

		echo '<h5 class="staff-name"><a href="'.get_the_permalink().'">'.get_the_title().'</a></h5>';

		if(!empty($title)) {
			echo '<h6 class="staff-title"><a href="'.get_the_permalink().'">'.$title.'</a></h6>';
		}
	echo '</div>';
