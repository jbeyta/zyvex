/**
 *                      *                      
 *    (    (  (       (  `                     
 *    )\   )\))(   '  )\))(      )  (          
 *  (((_) ((_)()\ )  ((_)()\  ( /(  )\   (     
 *  )\___ _(())\_)() (_()((_) )(_))((_)  )\ )  
 * ((/ __|\ \((_)/ / |  \/  |((_)_  (_) _(_/(  
 *  | (__  \ \/\/ /  | |\/| |/ _` | | || ' \)) 
 *   \___|  \_/\_/   |_|  |_|\__,_| |_||_||_|  
 *                                             
 */
// jquery stuff
jQuery(document).ready(function($){
	// uncomment below to support placeholders in < IE10
	// $('input, textarea').placeholder();

	// ie10 conditional, probably mostly gonna be used for flexbox centering fallback
	var doc = document.documentElement;
	doc.setAttribute('data-useragent', navigator.userAgent);

	var ie10 = false;
	var ie10_attr = $('html').attr('data-useragent');
	if(ie10_attr === 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0; .NET4.0E; .NET4.0C)') {
		ie10 = true;
	}

	// usage in css
	// html[data-useragent*='MSIE 10.0'] { css }

	$(".video-container").fitVids();

	///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	//
	//   _____      __  _                   _                 _ 
	//  / __\ \    / / | |   __ _ ____  _  | |   ___  __ _ __| |
	// | (__ \ \/\/ /  | |__/ _` |_ / || | | |__/ _ \/ _` / _` |
	//  \___| \_/\_/   |____\__,_/__|\_, | |____\___/\__,_\__,_|
	//	                             |__/                       
	//
	///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	// function cw_lload() {
	// 	$('.lload').each(function(){
	// 		var w_width = $(window).width();
	// 		var offset = $(this).offset();
	// 		var height = $(this).outerHeight();
	// 		var w_height = $(window).height();
	// 		var pos = Math.round(offset.top - $(window).scrollTop());
	// 		var load = w_height;
			
	// 		if(w_width <= 640) {
	// 			$(this).removeClass('lloaded');
	// 			$(this).find('.lchild').removeClass('lloaded-child');
	// 			return;
	// 		}

	// 		if(pos <= load) {
	// 			$(this).addClass('lloaded');

	// 			if($(this).hasClass('lchildren')) {
	// 				var lchildren = $(this).find('.lchild'),
	// 				i = 0,
	// 				lload_children = function() {
	// 					$(lchildren[i++]).addClass('lloaded-child');
	// 					if(i < lchildren.length) setTimeout(lload_children, 200);
	// 				}
	// 				lload_children();
	// 			}

	// 		} else {
	// 			$(this).removeClass('lloaded');
	// 			$(this).find('.lchild').removeClass('lloaded-child');
	// 		}
	// 	});
	// }


	///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	//
	//   _____      __    _                    _ _          
	//  / __\ \    / /   /_\  __ __ ___ _ _ __| (_)___ _ _  
	// | (__ \ \/\/ /   / _ \/ _/ _/ _ \ '_/ _` | / _ \ ' \ 
	//  \___| \_/\_/   /_/ \_\__\__\___/_| \__,_|_\___/_||_|
	//                                                      
	//
	///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	$('.cwa-section-header').each(function() {
		if($(this).hasClass('open-header')) {
			$(this).next('.cwa-section-content').slideDown(400).addClass('open-tab');
		}
	});

	$('.cwa-section-header').click(function(){
		
		if(!$(this).hasClass('open-header')) {
			$('.open-header').removeClass('open-header');
			$(this).addClass('open-header');

		} else if($(this).hasClass('open-header')) {
			$(this).removeClass('open-header');
		}

		$('.cwa-section-content').slideUp(400).removeClass('open-tab');

		if($(this).next('.cwa-section-content').is(':visible')){
			$(this).next('.cwa-section-content').slideUp(400).removeClass('open-tab');
		} else {
			$(this).next('.cwa-section-content').slideDown(400).addClass('open-tab');
		}
	});

	///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	//
	//  ___ _         _     _  _     _      _   _   
	// | _ ) |___  __| |__ | || |___(_)__ _| |_| |_ 
	// | _ \ / _ \/ _| / / | __ / -_) / _` | ' \  _|
	// |___/_\___/\__|_\_\ |_||_\___|_\__, |_||_\__|
	//                                |___/         
	//
	///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	// calculates block height and applies inline style
	// use for ie 10 flexbox centering fallback, etc.
	// function cw_block_height() {
	// 	$('.eqh-row').each(function(){
	// 		var window_w = $(window).width();
	// 		var block_h = 0;

	// 		if(window_w <= 640) {
	// 			$(this).find('.eqh-inner').removeAttr('style');
	// 			return;
	// 		}

	// 		// reset .inner so we can get the actual heights: the style attribute will override the actual height
	// 		$(this).find('.eqh-inner').removeAttr('style');

	// 		$(this).find('.eqh-inner').each(function(){
	// 			var compare_h = $(this).outerHeight();

	// 			if(compare_h > block_h) {
	// 				block_h = compare_h;
	// 			}
	// 		});

	// 		$(this).find('.eqh-inner').css({
	// 			'height': block_h + 'px'
	// 		});
	// 	});

	// 	var eqh_h = 0;
	// 	$('.eqh').each(function(){
	// 		$(this).removeAttr('style');
	// 		var this_h = $(this).outerHeight();

	// 		if(this_h > eqh_h) {
	// 			eqh_h = this_h;
	// 		}
	// 	});

	// 	$('.eqh').css({
	// 		'height': eqh_h + 'px'
	// 	});
	// }
	// cw_block_height();

	// hide captchas (gravity forms)
	$('.gform_wrapper').click(function(event){
		$(this).find('.gf_captcha').slideDown();
		event.stopPropagation();
	});

	$('html').click(function() {
		$('.gf_captcha').slideUp();
	});

	// nav toggle
	$('.menu-toggle').on('click, touchend', function() {
		var target = $(this).data('menu');
		$('.'+target).toggleClass('open');
		$('.'+target).slideToggle();
		// $('body').toggleClass('mobile-nav-open');
	});

	$('.mobnav').find('.sub-menu').slideUp();

	$('.mobnav').find('.menu-item-has-children').each(function(){
		$(this).on('touchend', function(){
			$('.sub-menu').each(function(){
				if($(this).is(':visible')) {
					$(this).slideUp();
				}
			});

			if(!$(this).find('.sub-menu').is(':visible')) {
				$(this).find('.sub-menu').slideDown();
			}
		});
	});

	// $('.menu-close').on('click, touchend', function() {
		// var target = $(this).data('menu');
		// $('.'+target).removeClass('open');
		// $('body').removeClass('mobile-nav-open');
	// });

	function menu_show_for_desktop() {
		var ww = $(window).width();
		var target =$('.menu-toggle').data('menu');
		if(ww > 640) {
			$('.'+target).addClass('open');
		}

		if(ww <= 640) {
			$('.'+target).removeClass('open');
		}
	}
	menu_show_for_desktop();

	function make_lightbox_gallery() {
		$('.gallery').each(function(){
			var id = $(this).attr('id');

			$(this).find('dl.gallery-item').each(function(){
				var caption = $(this).find('a').find('img').attr('alt');
				$(this).find('a').attr('data-lightbox', id);
				$(this).find('a').attr('data-title', caption);
			});
		});
	}

	// override gravity forms file upload appearance
	$('input[type="file"]').each(function(){
		var target = $(this).attr('id');
		$(this).css({
			'display': 'none'
		});
		$('[for='+target+']').addClass('button');

		$(this).change(function(e){
			// below taken from http://tympanus.net/codrops/2015/09/15/styling-customizing-file-inputs-smart-way/, may need to modify
			var fileName = '';
			if( this.files && this.files.length > 1 ) {
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			} else {
				fileName = e.target.value.split( '\\' ).pop();
			}

			if( fileName ) {
				$('[for='+target+']').html(fileName);
			}
		});
	});

	// cwtabs
	$('.cwtab-header').each(function(){
		if($(this).hasClass('current_page_item')) {
			var target = $(this).data('target');
			cwtabstabstabs(target);
		}
	});

	$('.cwtab-header').on('click touchend', function(){
		$('.current_page_item').removeClass('current_page_item');
		$(this).addClass('current_page_item');

		var target = $(this).data('target');
		cwtabstabstabs(target);

		console.log(target);
	});

	// var tabh = 0;

	// $('.tab-content').each(function(){
	// 	var h = $(this).outerHeight();

	// 	if(h > tabh) {
	// 		tabh = h;
	// 	}

	// 	$('.cwtabs-container').css({
	// 		'height': tabh+'px'
	// 	});
	// });

	function cwtabstabstabs(target) {
		$('.active_content').removeClass('active_content');

		$('#'+target).addClass('active_content');

		var h = $('.active_content').outerHeight();

		$('.cwtabs-container').css({
			'height': h+'px'
		});
	}

	// page sections nav
	// finds all elems of a class and makes a sidenav list item for each
	var pageSectNavItems = {};
	var i = 0;

	$('.page-sections-content').find('.page-section-header').each(function(){
		var navItem = {};
		var id = $(this).attr('id')

		if(id != undefined || id != null || id != '') {
			navItem.title = $(this).html();
			navItem.id = id;
		}

		pageSectNavItems[i] = navItem;

		i++;
	});

	if($('.page-sections-content').find('.page-section-header').length) {
		var newelems = $('<li class="page-sect-item menu-item"><a href="#overview">Overview</a></li>');
		$('.page-sections-nav').append(newelems);
	}

	for(var key in pageSectNavItems) {
		$('.page-sections-nav').append('<li class="page-sect-item menu-item"><a href="#'+pageSectNavItems[key]['id']+'">'+pageSectNavItems[key]['title']+'</a></li>');
	}

	$('.page-sections-nav').find('li:eq(0)').addClass('current_page_item');

	$('.page-sect-item').on('click touchend', function(e) {
		$('.page-sections-nav').find('li').removeClass('current_page_item');
		$(this).addClass('current_page_item');

		e.preventDefault();
		var target = $(this).find('a').attr('href');

		$('html, body').animate({
			scrollTop: $(target).offset().top - 45
		}, 500);
	});

	function show_back_to_top() {
		var pos = $(window).scrollTop();

		if(pos > 0) {
			$('.back-to-top').addClass('show');
		} else {
			$('.back-to-top').removeClass('show');
		}
	}

	$('.page-sect-item').on('click touchend', function(){
		$('.overview').addClass('current_page_item');
	});

	$(window).load(function(){
		$('.cw-slideshow').addClass('loaded');
		make_lightbox_gallery();
		show_back_to_top();

		// if( ie10 ) {
		// 	cw_block_height();
		// }	
	});

	$(window).resize(function(){
		menu_show_for_desktop();
		// if( ie10 ) {
		// 	cw_block_height();
		// }
	});

	$(window).scroll(function(){
		show_back_to_top();
	});
});