// originally from http://wptheming.com/2015/03/angular-json-api-example/

//////////
//
// NOTE: if search-app isn't working check this http://v2.wp-api.org/extending/custom-content-types/
//
//////////

var cwSearchApp = angular.module( 'cwSearchApp', ['ngSanitize'] );

// Set the configuration
cwSearchApp.run( ['$rootScope', function($rootScope) {

	// Variables defined by wp_localize_script
	// uncomment below or add a new cwSearchAppJS.[post_type]_api for each post type needed in the search
	$rootScope.allPostsAPI = [
		cwSearchAppJS.posts_api,
		cwSearchAppJS.pages_api,
		// cwSearchAppJS.tribe_events_api,
		// cwSearchAppJS.staff_api,
		// cwSearchAppJS.testimonials_api,
		// cwSearchAppJS.faqs_api,
		// cwSearchAppJS.links_api,
		// cwSearchAppJS.locations_api,
		// cwSearchAppJS.services_api,
		// cwSearchAppJS.galleries_api,
	];

}]);

// Add a controller
cwSearchApp.controller( 'searchAppController', ['$scope', '$http', function( $scope, $http ) {

	// console.log($scope.allPostsAPI);
	var arrayLength = $scope.allPostsAPI.length;
	$scope.posts = [];

	for (var i = 0; i < arrayLength; i++) {
		// console.log($scope.allPostsAPI[i]);

		// Load posts from the WordPress API
		$http({
			method: 'GET',
			url: $scope.allPostsAPI[i],
			params: {
				'filter[posts_per_page]' : -1
			},
		}).
		success( function( data, status, headers, config ) {
			var dataLength = data.length;
			for (var x = 0; x < dataLength; x++) {
				$scope.posts.push(data[x]);
			}
		}).
		error(function(data, status, headers, config) {});
	}

	$scope.reset = function(){
		var master = '';
		$scope.searchTerms = angular.copy(master);
		
		// give focus to the text input
		document.getElementById('s').focus();
	};
}]);