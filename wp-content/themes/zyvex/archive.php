<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="row">
			<div class="m9 m-push-3">
				<?php if ( have_posts() ) : ?>
					<?php /* Start the Loop */
					// use line below to change sort order
					// $posts = query_posts($query_string .'&orderby=title&order=asc&posts_per_page=-1'); 
					while ( have_posts() ) : the_post();
						get_template_part( 'content', get_post_format() );
					endwhile;
					?>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
			</div>

			<?php get_sidebar(); ?>	
		</div>
	</div>

<?php get_footer(); ?>