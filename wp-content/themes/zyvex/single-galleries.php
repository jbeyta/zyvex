<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="gallery" role="main">
		<div class="row">
			<div class="s12 main-content gallery-thumbnails">
				<?php
					while (have_posts()) {
						the_post();
						get_template_part('content', 'single-gallery');
					}
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
