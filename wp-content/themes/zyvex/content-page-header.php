<?php
	$extra_classes = '';

	if(is_singular('post')) {
		$extra_classes .= 'post-header';
	}
	
	$parent_id = $post->post_parent;
	$parent = get_post($parent_id);

	$shortHeader = false;

	if($parent->post_parent
		|| is_singular('contests')
		|| is_singular('galleries')
		|| is_singular('staff')
		|| is_singular('post')
		|| is_page('176')
		|| is_page('186')
	) {

		$extra_classes .= ' inside-page';

		$shortHeader = true;
	}
?>

<div class="page-header <?php echo $extra_classes; ?>" <?php echo $pbg; ?>>
	<div class="row">
		<div class="s12">
			<?php
				$title = get_the_title();
				if(!is_home() || !is_singular('post')) {
					echo '<h2 class="visually-hidden">'.$title.'</h2>';
				}

				$page_id = find_top_parent($post->ID);

				if(is_singular('staff')) {
					$page_id = 8;
				}

				if(is_singular('contests') || is_singular('galleries')) {
					$page_id = 28;
				}

				if(!empty($page_id)) {
					$title = get_the_title($page_id);
				}

				if(is_home() || is_singular('post')) {
					$newsid = get_option('page_for_posts');
					if(!empty($newsid)) {
						$title = get_the_title($newsid);
					}
				}

				echo '<h2 class="page-title">'.$title.'</h2>';

				$page_subtitle = get_post_meta($page_id, '_cwmb_subtitle', true);

				if($shortHeader) {
					$page_subtitle = '';
				}

				if(!empty($page_subtitle)) {
					echo '<h5 class="page-subtitle">'.$page_subtitle.'</h5>';
				}
			?>
		</div>
	</div>
</div>