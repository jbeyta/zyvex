<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */

// NOTE: if search-app isn't working check this http://v2.wp-api.org/extending/custom-content-types/
?>

	<div role="search" ng-app="cwSearchApp">
		<div ng-controller="searchAppController" >
			<div class="row">
				<div class="m12 columns">
					<input type="text" value="" name="s" placeholder="Search" id="s" ng-model="searchTerms" autocomplete="off">
				</div>
			</div>

			<!-- results conatiner -->
			<div class="search-results">
				<i class="fa fa-times close" aria-hidden="true" ng-show="posts.length > 1 && searchTerms" ng-click="reset()"></i>

				<i class="fa fa-refresh fa-spin fa-fw margin-bottom loading"  ng-show="posts.length < 1"></i>

				<div ng-repeat="post in posts | filter:searchTerms | limitTo:5" ng-show="searchTerms" class="result ng-cloak" >
					<a href="{{ post.link }}" ng-bind-html="post.title.rendered">{{ post.title.rendered }}</a>
				</div>

				<a href="/?s={{searchTerms}}" ng-show="searchTerms && (posts | filter:searchTerms).length != 0" class="more-results ng-cloak">More Results</a>
				<p class="no-results ng-cloak" ng-show="posts.length >= 1 && (posts | filter:searchTerms).length == 0 && searchTerms">No Results found for: {{searchTerms}}</p>
			</div>
		</div>
	</div>