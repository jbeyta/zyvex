<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?><!DOCTYPE html>

<!--[if lt IE 7]> <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html style="margin-top: 0!important;" class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html style="margin-top: 0!important;" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head >
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<?php
		$favicon = cw_options_get_option( '_cwo_favicon' );
		if(!empty($favicon)) { echo '<link rel="shortcut icon" type="image/png" href="'.$favicon.'"/>'; }
	?>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">

	<!-- WP_HEAD() -->
	<?php wp_head(); ?>

	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->

	<?php
		$tracking = cw_options_get_option( '_cwo_tracking_code' );
		if(!empty($tracking)) {
			echo $tracking;
		}
	?>

	<?php
		$pbg = '';
		$fpbg = '';

		$pageid = find_top_parent($post->ID);

		if(is_singular('staff')) {
			$pageid = 8;
		}

		if(is_singular('contests') || is_singular('galleries')) {
			$pageid = 28;
		};

		if(is_home()) {
			$pageid = get_option('page_for_posts');
		}

		$page_bg = get_post_meta($pageid, '_cwmb_bg_img', true);

		if(!empty($page_bg)) {
			$pbg = 'style="background-image: url('.$page_bg.'); background-position: center center; background-size: cover; background-repeat: no-repeat;"';
		}

		if(is_front_page() && !empty($page_bg)) {
			$pbg = '';
			$fpbg = 'style="background-image: url('.$page_bg.'); background-position: center center; background-size: cover; background-repeat: no-repeat;"';
		}
	?>
</head>

<body <?php body_class(); echo $fpbg; ?>>
	<div class="off-canvas-wrap">
		<div class="off-canvas-wrap-inner">
		<?php get_template_part('content', 'browse-happy'); ?>


		<header role="banner" <?php echo $pbg; ?>>
			<div class="cw-nav-cont row">
				<div class="flx-cont s12">
					<?php
						$logo_svg = cw_options_get_option( '_cwo_logo_svg' );
						$logo_url = cw_options_get_option( '_cwo_logo' );
					?>
					<h1 class="logo">
						<a href="/" title="<?php bloginfo( 'name' ); ?>">
							<?php if(!empty($logo_svg)) { ?>
								<?php echo $logo_svg; ?>
							<?php } elseif(!empty($logo_url)) { ?>
								<img src="<?php echo $logo_url; ?>" alt="<?php bloginfo( 'name' ); ?>" />
								<?php echo '<span class="visually-hidden">'.get_bloginfo('name').'</span>'; ?>
							<?php } else {
								bloginfo( 'name' );
							} ?>
						</a>
					</h1>

					<nav class="cw-nav" role="navigation">
					<!-- <span class="menu-close" data-menu="cw-nav-ul"><i class="fa fa-times"></i></span> -->
						<span class="menu-toggle" data-menu="cw-nav-ul"><i class="fa fa-bars"></i></span>
						
						
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'primary',
									'container' => '',
									'menu_class' => 'menu cf cw-nav-ul',
									'depth' => 2,
									'fallback_cb' => 'wp_page_menu',
									// 'walker' => new Foundation_Walker_Nav_Menu() // not required, use for custom nav stuff
								)
							);

							wp_nav_menu(
								array(
									'theme_location' => 'mobile',
									'container' => '',
									'menu_class' => 'menu mobnav cf cw-nav-ul',
									'depth' => 2,
									'fallback_cb' => 'wp_page_menu',
									// 'walker' => new Foundation_Walker_Nav_Menu() // not required, use for custom nav stuff
								)
							);
						?>
					</nav>
				</div>
			</div>

			<?php
				$showPageHeader = true;
				if(is_search() || is_404() || is_front_page()) {
					$showPageHeader = false;
				}

				if($showPageHeader) {
					get_template_part('content', 'page-header');
				}
			?>

			<?php if(empty($pbg) && !is_front_page()) { ?>
				<div class="header-bg"><?php get_template_part('img/svg/header', 'bg.svg'); ?></div>
			<?php } ?>
		</header>

		<?php
			if(is_singular('staff')) {
				$title = get_post_meta($post->ID, '_cwmb_staff_title', true);
				$image = get_post_meta($post->ID, '_cwmb_staff_image', true);
				$tab2_title = get_post_meta($post->ID, '_cwmb_staff_tab2_title', true);
				$tab3_title = get_post_meta($post->ID, '_cwmb_staff_tab3_title', true);
				$tab4_title = get_post_meta($post->ID, '_cwmb_staff_tab4_title', true);
				$tab5_title = get_post_meta($post->ID, '_cwmb_staff_tab5_title', true);
				$tab6_title = get_post_meta($post->ID, '_cwmb_staff_tab6_title', true);
				$tab7_title = get_post_meta($post->ID, '_cwmb_staff_tab7_title', true);

				$tabs = '';
				$tab_key = 1;

				if(!empty($tab2_title)) {
					$tabs .= '<li class="cwtab-header menu-item" data-target="tab-'.$tab_key.'"><a>'.$tab2_title.'</a></li>';
					$tab_key++;
				}
				if(!empty($tab3_title)) {
					$tabs .= '<li class="cwtab-header menu-item" data-target="tab-'.$tab_key.'"><a>'.$tab3_title.'</a></li>';
					$tab_key++;
				}
				if(!empty($tab4_title)) {
					$tabs .= '<li class="cwtab-header menu-item" data-target="tab-'.$tab_key.'"><a>'.$tab4_title.'</a></li>';
					$tab_key++;
				}
				if(!empty($tab5_title)) {
					$tabs .= '<li class="cwtab-header menu-item" data-target="tab-'.$tab_key.'"><a>'.$tab5_title.'</a></li>';
					$tab_key++;
				}
				if(!empty($tab6_title)) {
					$tabs .= '<li class="cwtab-header menu-item" data-target="tab-'.$tab_key.'"><a>'.$tab6_title.'</a></li>';
					$tab_key++;
				}
				if(!empty($tab7_title)) {
					$tabs .= '<li class="cwtab-header menu-item" data-target="tab-'.$tab_key.'"><a>'.$tab7_title.'</a></li>';
					$tab_key++;
				}

				echo '<div class="staff-header row">';
					echo '<div class="s6 m3">';
						if(!empty($image)) {
							$cropped = aq_resize($image, 640, 430, true, true, true);

							if(empty($cropped)) {
								$cropped = $image;
							}

							echo '<img class="staff-image" src="'.$cropped.'" alt="" />';
						}
					echo '</div>';

					echo '<div class="s6 m9">';
						echo '<h5 class="staff-name">'.get_the_title().'</h5>';
						echo '<h6 class="staff-title">'.$title.'</h6>';
					echo '</div>';	
				echo '</div>';

				echo '<div class="row sidenav-parents">';
					echo '<div class="s12">';

						if(!empty($tabs)) {
							echo '<ul class="cwtabs sidenav styleless">';
								echo '<li class="cwtab-header menu-item current_page_item" data-target="tab-0"><a>Biography</a></li>';
								echo $tabs;
							echo '</ul>';
						} else {
							echo '<ul class="cwtabs sidenav styleless">';
								echo '<li class="menu-item current_page_item"><a>Biography</a></li>';
							echo '</ul>';
						}

					echo '</div>';
				echo '</div>';

			} else { ?>

				<div class="row sidenav-parents s-hide">
					<div class="s12">
						<?php
							get_template_part('content', 'sidenav');
						?>
					</div>
				</div>

			<?php }
		?>



		<?php
			if(is_front_page()) {
				get_template_part('content', 'slides');
			}
		?>