<?php
	// child pages sidenav

	// we only want it running on pages, not posts
	if(is_page()) {
		// check if we're on a child page
		if($post->post_parent) {
			echo '<ul class="styleless sidenav child-page">';
				// make a nav item for the parent page
				echo '<li class="page_item page-item-'.$post->post_parent.'"><a href="'.get_the_permalink($post->post_parent).'">'.get_the_title($post->post_parent).'</a></li>';
				// list out siblings (children of this page's parent)
				wp_list_pages(array(
					'title_li' => '',
					'child_of' => $post->post_parent
				));
			echo '</ul>';
			// if we're on the parent page and then check if the page has children
		}
	}