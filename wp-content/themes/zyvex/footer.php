<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
			<footer role="contentinfo">
				<div class="row">
					<div class="s12 flx-cont">
						<?php
							$logo_svg = cw_options_get_option( '_cwo_logo_svg' );
							$logo_url = cw_options_get_option( '_cwo_logo' );
						?>
						<div class="logo">
							<?php if(!empty($logo_svg)) { ?>
								<?php echo $logo_svg; ?>
							<?php } elseif(!empty($logo_url)) { ?>
								<img src="<?php echo $logo_url; ?>" alt="<?php bloginfo( 'name' ); ?>" />
								<?php echo '<span class="visually-hidden">'.get_bloginfo('name').'</span>'; ?>
							<?php } else {
								bloginfo( 'name' );
							} ?>
						</div>

						<nav class="cw-nav" role="navigation">
							<?php
								wp_nav_menu(
									array(
										'theme_location' => 'footer',
										'container' => '',
										'menu_class' => 'footer-menu styleless cw-nav-ul',
										'depth' => 2,
										'fallback_cb' => 'wp_page_menu',
										'walker' => new Foundation_Walker_Nav_Menu() // not required, use for custom nav stuff
									)
								);
							?>
						</nav>
					</div>
				</div>

				<div class="row">
					<div class="m6 copy">
						<p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>, All Rights Reserved.</p>
					</div>
				</div>
			</footer>
		</div> <!-- end off-canvas-wrap-inner -->	
	</div> <!-- end off-canvas-wrap -->
	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
	<?php
		// must activate CW Options Page plugin for the line below to work
		$ga_code = cw_options_get_option('_cwo_ga'); if( !empty($ga_code) ) {

			// only put the tracking on code when the site is not on a .dev
			$extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);
			if($extension != 'dev') { ?>
				<script type="text/javascript">
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', '<?php echo $ga_code; ?>', 'auto');
					ga('send', 'pageview');
				</script>
			<?php }
		}
	?>
</body>
</html>