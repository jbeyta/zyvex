<?php
	$images = get_post_meta($post->ID, '_cwmb_gallery_images', true);
	$desc = get_post_meta($post->ID, '_cwmb_gallery_desc', true);

	if(!empty($desc)) {
		echo '<p>'.nl2br($desc).'</p>';
	}

	if(!empty($images)) {
		echo '<div class="row">';
		foreach ($images as $id => $img) {
			$imgpost = get_post($id);
			$caption = $imgpost->post_excerpt;

			$cropped = aq_resize($img, 300, 300, true, true, true);

			echo '<div class="s6 m4 l3 gallery-thumb">';
				echo '<a href="'.$img.'" data-lightbox="gallery-'.$post->ID.'" data-title="'.$caption.'">';
					echo '<img class="gallery-image" src="'.$cropped.'" alt="'.$caption.'" />';
				echo '</a>';
			echo '</div>';
		}
		echo '</div>';
	}