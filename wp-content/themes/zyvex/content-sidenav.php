<?php
	if(is_page() || is_singular('staff') || is_singular('contests') || is_singular('galleries')) {
		$top_id = find_top_parent($post->ID);

		if(is_singular('staff')) {
			$top_id = 8;
		}

		if(is_singular('contests') || is_singular('galleries')) {
			$top_id = 28;
		}

		if(is_page(176) || is_page(186)) {
			$top_id = 16;
		}

		if(!empty($top_id)) {
			echo '<ul class="styleless sidenav child-page">';
				wp_list_pages(array(
					'title_li' => '',
					'child_of' => $top_id,
					'depth' => 1
				));
			echo '</ul>';
		}
	}