<?php
/**
 * Template Name: Contact Page Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="main" role="main">
		<div class="row">
			<div class="m9 m-push-3">
				<?php the_post_thumbnail() ?>

				<?php echo do_shortcode('[contact_info address="hide" map="show"]'); ?>

				<?php if (have_posts()) : while (have_posts()) : the_post();
					the_content();
				endwhile; endif; ?>
			</div>

			<aside class="widget-area m-pull-9 m3" role="complementary">
				<?php echo do_shortcode('[contact_info phone="show" email="show"]'); ?>
			</aside>
		</div>
	</div>

<?php get_footer(); ?>