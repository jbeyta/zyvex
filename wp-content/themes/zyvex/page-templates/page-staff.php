<?php
/**
 * Template Name: Staff Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="staff" role="main">
		<div class="row staff-container">
			<?php if (have_posts()) : while (have_posts()) : the_post();
				the_content();
			endwhile; endif; ?>

			<?php 
				$post_type = 'staff';
				$post_args = array(
					'post_type' => $post_type,
					'posts_per_page' => -1,
					'orderby' => 'menu_order',
					'order' => 'ASC'
				);

				$posts = new WP_Query($post_args);
				if($posts->have_posts()){
					while($posts->have_posts()){
						$posts->the_post();
						get_template_part('content', 'staff');

					}
				} else {
					echo '<p>No '.$post_type.' yet. Check back soon</p>';
				}

				wp_reset_query();
			?>
		</div>
	</div>

<?php get_footer(); ?>