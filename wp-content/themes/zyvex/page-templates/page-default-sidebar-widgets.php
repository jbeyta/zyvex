<?php
/**
 * Template Name: Page with default widget sidebar Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main" role="main">
		<div class="row">
			<div class="m9 m-push-3">
				<?php
					if(have_posts()) {
						while(have_posts()) {
							the_post();

							the_content();
							// comments_template( '', true );
						}
					}
				?>
			</div>

			<?php get_sidebar(); ?>
		</div>

	</div>

<?php get_footer(); ?>