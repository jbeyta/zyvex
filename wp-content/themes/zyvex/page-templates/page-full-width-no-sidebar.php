<?php
/**
 * Template Name: Full Width, No Sidebar Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main" role="main">
		<div class="row">
			<div class="s12">
				<?php
					if(have_posts()) {
						while(have_posts()) {
							the_post();
							the_content();
						}
					}
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>