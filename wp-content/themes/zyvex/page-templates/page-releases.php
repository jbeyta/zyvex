<?php
/**
 * Template Name: Press Releases Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="links" role="main">
		<div class="row">
			<div class="s12">
				<?php /*if (have_posts()) : while (have_posts()) : the_post();
					the_content();
				endwhile; endif; */?>
				<?php 
					$post_type = 'press_releases';
					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
					$post_args = array(
						'post_type' => $post_type,
						'posts_per_page' => 10,
						'orderby' => 'date',
						'order' => 'DESC',
						'paged' => $paged
					);

					$posts = new WP_Query($post_args);
					if($posts->have_posts()){
						while($posts->have_posts()){
							$posts->the_post();
							echo '<div class="press-release">';
								echo '<h4 class="title">'.get_the_title().'</h4>';
								the_content();
							echo '</div>';
						}

						if (function_exists('pagination')) {
							pagination($posts->max_num_pages);
						}
					} else {
						echo '<p>No '.$post_type.' yet. Check back soon';
					}

					wp_reset_query();
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>