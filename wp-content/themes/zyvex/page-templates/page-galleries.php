<?php
/**
 * Template Name: Galleries Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="galleries" role="main">
		<div class="row">
			<div class="s12 galleries-cont">
				<?php if (have_posts()) : while (have_posts()) : the_post();
					the_content();
				endwhile; endif; ?>

				<?php
					$post_type = 'galleries';
					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
					$post_args = array(
						'post_type' => $post_type,
						'posts_per_page' => 9,
						'paged' => $paged,
						'orderby' => 'date',
						'order' => 'DESC'
					);

					$posts = new WP_Query($post_args);
					if($posts->have_posts()){
						while($posts->have_posts()){
							$posts->the_post();
							get_template_part('content', $post_type);
						}
					} else {
						echo '<p>No '.$post_type.' yet. Check back soon</p>';
					}

					if (function_exists('pagination')) {
						echo '<div class="s12 pagination-cont">';
							pagination($posts->max_num_pages);
						echo '</div>';
					}

					wp_reset_query();
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
