<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main" role="main">
		<div class="page-header-variant">
			<div class="row">
				<div class="s12">
					<h2 class="entry-title">Not Found: 404</h2>
					<p>Content not found. Please make sure the url is correct.</p>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>